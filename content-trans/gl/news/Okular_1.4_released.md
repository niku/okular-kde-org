---
date: 2018-04-19
title: Publicouse Okular 1.4
---
A versión 1.4 de Okular publicouse coa versión 18.04 das aplicacións de KDE. Esta versión introduce unha mellora da compatibilidade con JavaScript en ficheiros PDF e permite cancelar a renderización de PDF, o que significa que se ten un ficheiro PDF complexo e cambia a ampliación mentres está a renderizarse, esta cancelarase inmediatamente en vez de esperar a que remate a renderización. Pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. Okular 1.4 é unha actualización recomendada para calquera que use Okular.
