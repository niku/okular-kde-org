---
date: 2017-04-20
title: Publicouse Okular 1.1
---
A versión 1.1 de Okular publicouse xunto coa versión 17.04 das aplicacións de KDE. Esta versión introduce a funcionalidade de cambio de tamaño das anotacións, funcionalidade de cálculo automático do contido de formularios, melloras para pantallas táctiles, e máis! Pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.1 é unha actualización recomendada para calquera que use Okular.
