---
date: 2009-05-06
title: Publicouse Okular 0.8.3
---
A terceira versión de mantemento da serie 4.2 de KDE inclúe Okular 0.8.3. Non fornece moitas novidades para Okular, o único cambio relevante é o uso de fíos con maior seguranza ao xerar imaxes de páxinas de documentos XPS. Pode consultar todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
