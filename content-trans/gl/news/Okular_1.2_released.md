---
date: 2017-08-17
title: Publicouse Okular 1.2
---
A versión 1.2 de Okular publicouse coa versión 17.08 das aplicacións de KDE. Esta versión introduce pequenas melloras e solucións de erros. Pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Okular 1.2 é unha actualización recomendada para calquera que use Okular.
