---
date: 2010-08-10
title: Ilmus Okular 0.11
---
Okulari versioon 0.11 ilmus koos KDE rakenduste väljalaskega 4.5. See väljalase sisaldab väiksemaid parandusi ja uusi võimalusi ning kõigil, kes kasutavad Okulari, on soovitatav see kasutusele võtta.
