---
date: 2008-03-05
title: Ilmus Okular 0.6.2
---
KDE 4.0 seeria teine hooldusväljalase sisaldab Okulari 0.6.2. See sisaldab mõningaid veaparandusi, sealhulgas suuremat stabiilsust dokumendi sulgemisel, ja väiksemaid parandusi järjehoidjate süsteemis. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
