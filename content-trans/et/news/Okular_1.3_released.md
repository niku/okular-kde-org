---
date: 2017-12-14
title: Ilmus Okular1.3
---
Okulari versioon 1.3 ilmus koos KDE rakenduste väljalaskega 17.12. See väljalase sisaldab muudatusi annotatsioonide ja vormiandmete salvestamises, toetab osaliselt renderdamise uuendamist failide puul, mille renderdamine võtab kaua aega, muudab teksti valimise režiimis tekstilingid interaktiivseks, lisab menüüsse Fail käsu Jaga, pakub Markdowni toetust ja parandab mõned probleemid HiDPI toetusega. Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.3 peale.
