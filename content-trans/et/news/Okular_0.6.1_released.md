---
date: 2008-02-05
title: Ilmus Okular 0.6.1
---
KDE 4.0 seeria esimene hooldusväljalase sisaldab Okulari 0.6.1. See sisaldab mõningaid veaparandusi, sealhulgas paremat failide taasallalaadimisest loobumist salvestamisel, kasutusmugavuse parandusi jms. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
