---
date: 2020-04-23
title: Ilmus Okular 1.10
---
Ilmus Okulari versioon 1.10. See väljalase pakub kineetilist kerimist, kaardihalduse täiustusi, mobiili kasutajaliidese täiustusi ning muid vähema tähendusega omadusi ja parandusi. Täielikku muutuste logi näeb aadressil <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.10 peale.
