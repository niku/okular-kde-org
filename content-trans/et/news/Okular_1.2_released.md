---
date: 2017-08-17
title: Ilmus Okular 1.2
---
Okulari versioon 1.2 ilmus koos KDE rakenduste väljalaskega 17.08. See väljalase sisaldab väiksemaid parandusi ja täiustusi. Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.2 peale.
