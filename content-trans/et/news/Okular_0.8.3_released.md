---
date: 2009-05-06
title: Ilmus Okular 0.8.3
---
KDE 4.2 seeria kolmas hooldusväljalase sisaldab Okulari 0.8.3. See ei too Okulari osas kaasa erilisi muutusi, ainuke olulisem on lõimeturvalisuses suurendamine XPS-dokumendi leheküljepiltide genereerimisel. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
