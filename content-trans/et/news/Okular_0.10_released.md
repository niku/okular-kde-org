---
date: 2010-02-09
title: Ilmus Okular 0.10
---
Okulari versioon 0.10 ilmus koos KDE tarkvarakomplektiga 4.4. Lisaks üldisele stabiilsuse parandamisele pakub see väljalase uut või parandatud tuge nii pöörd- kui ka edasiotsingule, seostades LaTeXi lähekoodifaili read vastavate asukohtadega dvi- ja pdf-failides.
