---
date: 2013-12-12
title: O Okular 0.18 foi lançado
---
A versão 0.18 do Okular foi lançada em conjunto com a versão 4.12 das Aplicações do KDE. Esta versão introduz novas funcionalidades, como o suporte de áudio/vídeo para os ficheiros ePub, assim como melhorias na pesquisa e impressão. É uma actualização recomendada para todos os que usam o Okular.
