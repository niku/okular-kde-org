---
date: 2021-08-12
title: O Okular 21.08 foi lançado
---
A versão 21.08 do Okular foi lançada. Esta versão introduz a Assinatura Digital de ficheiros PDF e diversas correcções e funcionalidades pequenas em todo o lado. Poderá verificar o registo de alterações completo em <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https://kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
