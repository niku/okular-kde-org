---
date: 2008-03-05
title: O Okular 0.6.2 foi lançado
---
A segunda versão de manutenção da série KDE 4.0 inclui o Okular 0.6.2. Este inclui algumas correcções de erros, incluindo uma melhor estabilidade ao fechar um documento, assim como algumas pequenas correcções para o sistema de marcação de favoritos. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
