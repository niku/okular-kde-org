---
date: 2013-08-16
title: O Okular 0.17 foi lançado
---
A versão 0.17 do Okular foi lançada em conjunto com a versão 4.11 das Aplicações do KDE. Esta versão introduz novas funcionalidades, como o suporte para desfazer/refazer em formulários e anotações, assim como ferramentas de revisão configuráveis. É uma actualização recomendada para todos os que usam o Okular.
