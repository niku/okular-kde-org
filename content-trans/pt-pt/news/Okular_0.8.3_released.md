---
date: 2009-05-06
title: O Okular 0.8.3 foi lançado
---
A terceira versão de manutenção da série KDE 4.2 inclui o Okular 0.8.3. Não traz muitas novidades para o Okular, sendo a única alteração relevante a maior segurança ao gerar as imagens das páginas dos documentos XPS. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
