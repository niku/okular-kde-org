---
intro: O Okular suporta uma grande variedade de formatos de documentos e casos de
  uso.
layout: formats
menu:
  main:
    name: Formato do Documento
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Estado do Tratamento de Formatos de Documentos
---
