---
date: 2013-12-12
title: Vydaný Okular 0.18
---
The 0.18 version of Okular has been released together with KDE Applications 4.12 release. This release introduces new features like audio/video support in EPub files and also improvements to existing features like searching and printing. Okular 0.18 is a recommended update for everyone using Okular.
