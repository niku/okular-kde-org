---
intro: У Okular передбачено підтримку багатьох форматів документів та режимів перегляду.
layout: formats
menu:
  main:
    name: Формат документів
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Стан розробки інструментів обробки форматів документів
---
