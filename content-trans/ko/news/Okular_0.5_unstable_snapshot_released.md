---
date: 2006-08-27
title: Okular 0.5 불안정 스냅샷 출시
---
Okular 팀은 <a href="http://dot.kde.org/1155935483/">첫 번째 KDE 4 'Krash' 스냅샷</a>으로 컴파일할 수 있는 Okular 스냅샷을 발표합니다. 이 스냅샷은 아직 완벽하게 작동하지는 않으며, 가다듬고 마무리해야 할 많은 것이 있지만, 테스트 목적의 사용과 피드백은 얼마든지 환영합니다. 스냅샷 패키지를 다운로드하려면 <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2</a> 페이지를 확인하십시오. <a href="download.php">다운로드</a> 페이지에서 필수 라이브러리 목록을 확인할 수 있습니다.
