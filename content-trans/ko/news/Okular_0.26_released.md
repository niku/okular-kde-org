---
date: 2016-08-18
title: Okular 0.26 출시
---
Okular의 0.26 버전은 KDE 프로그램 16.08 릴리스와 함께 출시되었습니다. 이 릴리스에서 변경된 사항은 매우 적으므로 전체 변경 내역을 확인하려면 <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a> 페이지를 확인하십시오. Okular 0.26은 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
