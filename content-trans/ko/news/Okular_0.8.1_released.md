---
date: 2009-03-04
title: Okular 0.8.1 출시
---
KDE 4.2 시리즈의 첫 번째 유지 관리 릴리스에는 Okular 0.8.1이 들어 있습니다. 이 릴리스에는 CHM 및 DjVu 백엔드의 일부 충돌 개선과 사용자 인터페이스의 사소한 개선이 들어 있습니다. 전체 변경 사항을 보려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a> 페이지를 방문하십시오
