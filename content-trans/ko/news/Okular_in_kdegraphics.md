---
date: 2007-04-04
title: kdegraphics의 Okular
---
Okular 팀은 Okular가 kdegraphics 모듈에 포함되었음을 발표합니다. Okular의 다음 버전은 KDE 4.0과 함께 제공됩니다.
