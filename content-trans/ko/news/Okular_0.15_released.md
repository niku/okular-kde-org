---
date: 2012-08-01
title: Okular 0.15 출시
---
Okular 0.15 버전은 KDE 프로그램 4.9 릴리스와 함께 출시되었습니다. 이 릴리스에서는 PDF 문서에 주석 저장, PDF 동영상 추가 지원, 한 쪽에 여러 책갈피 지원 및 기타 기능 개선이 있었습니다. 전체 버그 및 기능 목록을 확인하려면 <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=okular&amp;long_desc_type=allwordssubstr&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=exact&amp;email1=&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2010-10-10&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.9.0&amp;cmdtype=doit&amp;order=Reuse+same+sort+as+last+time&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">bugzilla</a> 페이지를 확인하십시오. Okular 0.15는 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
