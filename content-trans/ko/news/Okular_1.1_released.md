---
date: 2017-04-20
title: Okular 1.1 출시
---
Okular 1.1 버전은 KDE 프로그램 17.04와 함께 출시되었습니다. 이 릴리스에서는 주석 크기 조정 기능, 양식 내용 자동 계산 지원, 터치 스크린 개선 등이 추가되었습니다! 전체 변경 내역을 확인하려면 <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a> 페이지를 방문하십시오. Okular 1.1은 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
