---
date: 2010-08-10
title: Okular 0.11 출시
---
Okular의 0.11 버전은 KDE 프로그램 4.5 릴리스와 함께 출시되었습니다. 이 릴리스에는 작은 수정 사항과 기능 개선이 들어 있으며 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
