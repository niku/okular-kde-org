---
date: 2007-07-10
title: aKademy 2007 프레젠테이션 자료 업로드됨
---
<a href="http://akademy2007.kde.org">aKademy 2007</a>에서 Pino Toscano가 발표한 Okular 발표가 업로드되었습니다. <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">슬라이드</a>와 <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">비디오</a> 가 모두 올라와 있습니다.
