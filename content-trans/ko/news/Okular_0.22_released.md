---
date: 2015-04-15
title: Okular 0.22 출시
---
Okular의 0.22 버전은 KDE 프로그램 15.04와 함께 출시되었습니다. Okular 0.22는 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
