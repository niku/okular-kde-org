---
menu:
  main:
    parent: about
    weight: 4
title: 연락하기
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, KDE 마스코트" style="height: 200px;"/>

Okular 팀에 여러 가지 방법으로 문의할 수 있습니다:

* 메일링 리스트: Okular 개발은 kde.org의 [okular-devel 메일링 리스트](https://mail.kde.org/mailman/listinfo/okular-devel)에서 논의하고 있습니다. 리스트에서 코어 애플리케이션 개발에 대해 이야기하는 것 외에도 기존 또는 새로운 백엔드에 대한 피드백을 받고 있습니다.

* IRC: 일반적인 대화는 [Freenode IRC 네트워크](http://www.freenode.net/)의 [#okular](irc://irc.kde.org/#okular) 및 [#kde-devel](irc://irc.kde.org/#kde-devel) 채널에서 진행됩니다. 일부 Okular 개발자를 만날 수 있습니다.

* Matrix: 위 IRC 채널은 Matrix 네트워크의 [#okular:kde.org](https://matrix.to/#/#okular:kde.org)로도 접근할 수 있습니다.

* 포럼: 포럼에서 대화하고 싶으시다면 [KDE 커뮤니티 포럼](http://forum.kde.org)의 하위 포럼인 [Okular 포럼](http://forum.kde.org/viewforum.php?f=251)을 사용할 수 있습니다.

* 버그와 요청 사항: 버그와 요청 사항은 [KDE 버그 트래커](https://bugs.kde.org)에 보고해 주십시오. 기능 개선에 기여하고 싶으면 [여기](https://community.kde.org/Okular)에 있는 우선 순위가 높은 버그를 확인할 수 있습니다..
