---
intro: O Okular suporta uma grande variedade de formatos de documentos e casos de
  uso.
layout: formats
menu:
  main:
    name: Formato do documento
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Estado do tratamento de formatos de documentos
---
