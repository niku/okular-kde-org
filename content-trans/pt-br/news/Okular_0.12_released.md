---
date: 2011-01-26
title: O Okular 0.12 foi lançado
---
A versão 0.12 do Okular foi lançada junto com a versão 4.6 do KDE Applications. Esta versão introduz algumas pequenas correções e funcionalidades, e é uma atualização recomendada a todos os usuários do Okular.
