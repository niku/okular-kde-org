---
date: 2006-11-02
title: A versão instável do Okular 0.5.81 foi lançada
---
A equipa do Okular está orgulhosa em anunciar o lançamento de uma versão do Okular que compila contra a <a href="http://dot.kde.org/1162475911/">Segunda Versão de Desenvolvimento do KDE 4</a>. Esta imagem ainda não está completamente funcional; para além disso, ainda temos montes de coisas para polir e terminar, mas está completamente à vontade para o testar e dar o máximo de reacções a ele que desejar. Poderá encontrar o pacote instável em <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Dê uma vista de olhos na página de <a href="download.php">transferências</a> para garantir que tem todas as bibliotecas necessárias.
