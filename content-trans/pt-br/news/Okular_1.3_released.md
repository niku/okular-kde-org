---
date: 2017-12-14
title: O Okular 1.3 foi lançado
---
A versão 1.3 do Okular foi lançada junto com o KDE Applications versão 17.12. Esta versão introduz alterações no salvamento de anotações e como os formulários funcionam, suporte parcial a atualizações de renderização para arquivos que demoram muito para renderizar, torna links de texto interativos no modo de seleção de texto, adiciona uma opção de compartilhar no menu Arquivo, adiciona suporta ao Markdown e corrige alguns problemas com relação ao suporte HiDPI. Você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. O Okular 1.3 é uma atualização recomendada a todos os usuários do Okular.
