---
date: 2007-07-10
title: Material on-line de apresentações do Okular no aKademy 2007
---
A apresentação do Okular, feita pelo Pino Toscano na <a href="http://akademy2007.kde.org">aKademy 2007</a> está agora disponível. Existem <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">apresentações</a> e um <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">vídeo</a> disponíveis.
