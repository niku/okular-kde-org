---
date: 2015-08-19
title: O Okular 0.23 foi lançado
---
A versão 0.23 do Okular foi lançada junto com o KDE Applications 15.08. Esta versão introduz o suporte a transições com desaparecimento no modo de apresentação, assim como algumas correções relacionadas a reprodução de vídeo e anotações. É uma atualização recomendada a todos os usuários do Okular.
