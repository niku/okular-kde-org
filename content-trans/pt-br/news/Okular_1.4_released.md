---
date: 2018-04-19
title: O Okular 1.4 foi lançado
---
A versão 1.4 do Okular foi lançada junto com o KDE Applications versão 18.04. Esta versão introduz um suporte melhorado à JavaScript nos arquivos PDF e suporte ao cancelamento de renderização de PDFs, que significa que se você tiver um arquivo PDF complexo e alterar o zoom enquanto ele estiver renderizando, ele irá cancelar imediatamente ao invés de aguardar a renderização terminar. Você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>.  O Okular 1.4 é uma atualização recomendada para todos os usuários do Okular.
