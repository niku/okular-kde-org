---
intro: O Okular está disponível como um pacote pré-compilado numa grande gama de plataformas.
  Poderá verificar o estado dos pacotes da sua distribuição de Linux à direita ou
  continue a ler mais informações sobre outros sistemas operativos
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: O Okular já está disponível na maioria das distribuições de Linux. Podê-lo-á
    instalar no [Centro de Aplicações do KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logotipo do Flatpak
  name: Flatpak
  text: Poderá instalar a última versão do [Flatpak do Okular](https://flathub.org/apps/details/org.kde.okular)
    do Flathub. Os Flatpak's experimentais com versões diárias do Okular podem ser
    [instalados a partir do repositório de Flatpak do KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Logotipo do Ark
  name: Código das Versões
  text: O Okular é lançado com regularidade como parte do KDE Gear. Se quiser compilar
    a partir do código, poderá consultar a [secção Compilar](/build-it).
- image: /images/windows.svg
  image_alt: Logotipo do Windows
  name: Windows
  text: Para o Windows, dê uma vista de olhos na página Web [Iniciativa do KDE no
    Windows](https://community.kde.org/) para saber mais informações como instalar
    o KDE no Windows. A versão estável está disponível na [Loja da Microsoft](https://www.microsoft.com/store/apps/9N41MSQ1WNM8).
    Também existem [versões experimentais nocturnas](https://binary-factory.kde.org/job/Okular_Nightly_win64/).
    Por favor teste e envie modificações.
sassFiles:
- /sass/download.scss
title: Download
---
## Instalação

Para instalar o Okular, siga as instruções da plataforma que está usar para instalar as aplicações. A maioria das plataformas tem uma interface gráfica que poderá ser usada para instalar os pacotes das aplicações. Na maioria dos casos, a mesma será aberta automaticamente quando carregar na hiperligação de transferência ou instalação da sua plataforma.

Se estiver a usar o Linux, o Okular provavelmente virá já pré-instalado no seu sistema, caso faça parte de uma selecção predefinida. Caso contrário, podê-lo-á instalar com a ferramenta de gestão de pacotes da sua distribuição de Linux. Veja mais detalhes na sua documentação.

Na maioria dos casos, terá a oportunidade de instalar o Okular de forma modular, para que possa decidir a instalação do suporte para determinados formatos em separados ou traduções para a interface do utilizador. Escolha os módulos de acordo com as suas necessidades e preferências.

## Desinstalação

Para desinstalar o Okular, siga as instruções da ferramenta de gestão de pacotes que usou para instalar o Okular. Isto irá remover a aplicação do seu sistema. Não irá remover nenhuns dados do utilizador que tenham sido vistos, criados ou modificados com o Okular.
