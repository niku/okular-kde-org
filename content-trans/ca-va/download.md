---
intro: L'Okular està disponible com a paquet precompilat en una gran varietat de plataformes.
  Podeu comprovar l'estat del paquet per a la vostra distribució de Linux de la dreta,
  o continueu llegint per a informació en altres sistemes operatius
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: L'Okular ja està disponible a la majoria de les distribucions de Linux. El
    podeu instal·lar des del [Centre de programari de KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logotip del Flatpak
  name: Flatpak
  text: Podeu instal·lar el darrer [Flatpak de l'Okular](https://flathub.org/apps/details/org.kde.okular)
    des de Flathub. Els Flatpaks experimentals amb les construccions nocturnes de
    l'Okular es poden [instal·lar des del repositori Flatpak de KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Logotip de l'Ark
  name: Codi font del llançament
  text: L'Okular es publica amb regularitat com a part de KDE Gear. Si el voleu construir
    a partir del codi font, podeu revisar la [secció Construcció](/build-it).
- image: /images/windows.svg
  image_alt: Logotip del Windows
  name: Windows
  text: Doneu una ullada a la [iniciativa de KDE al Windows](https://community.kde.org/Windows)
    per a informació sobre com instal·lar el programari KDE al Windows. La publicació
    estable està disponible a la [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    També hi ha [compilacions nocturnes experimentals](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    per a les quals s'agraeixen les proves i els informes d'errors.
sassFiles:
- /sass/download.scss
title: Baixada
---
## Instal·lació

Per a instal·lar l'Okular seguiu les instruccions de la plataforma que useu quant a la instal·lació de programari. La majoria de les plataformes tenen un instal·lador gràfic que es pot utilitzar per a instal·lar paquets de programari. En molts casos s'obrirà automàticament en fer clic a l'enllaç de baixada o instal·lació de la vostra plataforma.

Si empreu el Linux, l'Okular ja podria estar preinstal·lat en el sistema com a part de la selecció predeterminada. Si no, el podeu instal·lar amb l'eina de gestió de paquets de la vostra distribució Linux. Vegeu la seua documentació per als detalls.

En molts casos tindreu l'oportunitat d'instal·lar l'Okular d'una manera modular, de manera que podreu decidir instal·lar el funcionament per a formats específics separadament o instal·lar les traduccions per a la interfície d'usuari. Trieu els mòduls segons les vostres necessitats i preferències.

## Desinstal·lació

Per a desinstal·lar l'Okular seguiu les instruccions de l'eina de gestió de paquets que heu usat per a instal·lar l'Okular. Això eliminarà l'aplicació Okular. No tocarà les dades que heu vist, creat o modificat amb l'Okular.
