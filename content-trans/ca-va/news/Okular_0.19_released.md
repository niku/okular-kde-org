---
date: 2014-04-16
title: Publicat l'Okular 0.19
---
S'ha publicat la versió 0.19 de l'Okular conjuntament amb la publicació 4.13 de les Aplicacions del KDE. Aquesta publicació presenta característiques noves com l'admissió de pestanyes a la interfície, l'ús de ppp de pantalla de manera que la mida de la pàgina coincidisca amb la mida real del paper, millores a l'entorn de treball de Desfer/Refer i altres característiques/refinaments. L'Okular 0.19 és una actualització recomanada per a tothom que usi l'Okular.
