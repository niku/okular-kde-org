---
date: 2007-07-10
title: Material en línia de la presentació de l'Okular a l'aKademy 2007
---
La presentació de l'Okular efectuada pel Pino Toscano a l'<a href="http://akademy2007.kde.org">aKademy 2007</a> ara està en línia. Hi ha disponible tant les <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">diapositives</a> com el <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">vídeo</a>.
