---
date: 2017-12-14
title: Publicat l'Okular 1.3
---
S'ha publicat la versió 1.3 de l'Okular conjuntament amb la publicació 17.12 de les Aplicacions del KDE. Aquesta publicació presenta canvis en com es guarden les anotacions i el funcionament de les dades de formularis, admet actualitzacions parcials de la renderització per a fitxers que triguen a renderitzar, fa interactius els enllaços de text en mode de selecció de text, afig l'opció de compartir al menú Fitxer, afig la implementació per al Markdown i esmena diversos problemes referits a la implementació del HiDPI. Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. L'Okular 1.3 és una actualització recomanada per a tothom que usi l'Okular.
