---
date: 2019-08-15
title: Publicat l'Okular 1.8
---
S'ha publicat la versió 1.8 de l'Okular conjuntament amb la publicació 19.08 de les Aplicacions del KDE. Aquesta publicació introdueix la característica per a configurar les finalitzacions de l'anotació de línia entre altres esmenes i característiques petites. Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. L'Okular 1.8 és una actualització recomanada per a tothom que usi l'Okular.
