---
intro: Okular supports a wide variety of document formats and use cases.
layout: formats
menu:
  main:
    name: မှတ်တမ်းပုံနှိပ်မူသိမ်းဆည်းပုံစံ
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: မှတ်တမ်းပုံနှိပ်မူသိမ်းဆည်းပုံစံ ကိုင်တွယ်မှုအခြေအနေ
---
