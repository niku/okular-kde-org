---
date: 2013-08-16
title: Publicat l'Okular 0.17
---
S'ha publicat la versió 0.17 de l'Okular conjuntament amb la publicació 4.11 de les Aplicacions del KDE. Aquesta publicació presenta característiques noves com la implementació de desfer/refer per als formularis i anotacions, i eines configurables de revisió. L'Okular 0.17 és una actualització recomanada per a tothom que usi l'Okular.
