---
date: 2006-11-02
title: Publicat l'Okular 0.5.81 en versió instantània inestable
---
L'equip de l'Okular anuncia amb satisfacció la publicació d'una versió instantània de l'Okular que compila contra la <a href="http://dot.kde.org/1162475911/">Second KDE 4 Developers Snapshot</a>. Aquesta instantània encara no és completament funcional, ja que hi ha moltes coses per a polir i finalitzar, però sou lliure de provar-la i proporcionar els comentaris que vulgueu. Trobareu el paquet de la instantània a <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Doneu una ullada a la pàgina de <a href="download.php">baixada</a> per a assegurar que teniu totes les biblioteques necessàries.
