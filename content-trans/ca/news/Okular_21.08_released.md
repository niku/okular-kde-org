---
date: 2021-08-12
title: Publicat l'Okular 21.08
---
S'ha publicat la versió 21.08 de l'Okular. Aquest llançament conté diverses correccions i característiques secundàries. Podeu revisar el registre complet de canvis a <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https://kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. L'Okular 21.08 és una actualització recomanada per a tothom que usi l'Okular.
