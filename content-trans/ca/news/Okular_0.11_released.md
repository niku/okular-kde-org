---
date: 2010-08-10
title: Publicat l'Okular 0.11
---
S'ha publicat la versió 0.11 de l'Okular conjuntament amb la publicació 4.5 de les Aplicacions del KDE. Aquesta publicació presenta petites esmenes d'errors i característiques i és una actualització recomanada per a tothom que usi l'Okular.
