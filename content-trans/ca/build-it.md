---
menu:
  main:
    name: Construcció
    parent: about
    weight: 2
title: Compilació de l'Okular a partir del codi font en el Linux
---
<span style="background-color:#e8f4fa">Si esteu cercant els paquets precompilats, visiteu la [pàgina de baixades](/download/). Podreu comprovar l'estat de l'empaquetament [aquí](https://repology.org/project/okular/versions)</span>

Si voleu compilar l'Okular, cal tenir establert un entorn de compilació, que en general, l'hauria de proporcionar la vostra distribució. En cas que vulgueu compilar la versió de desenvolupament de l'Okular, vegeu la construcció a partir del codi font al Wiki de la Comunitat KDE.

Podeu baixar i compilar l'Okular d'aquesta manera:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Si instal·leu l'Okular a un camí diferent del directori d'instal·lació del sistema, és possible que calgui executar `source build/prefix.sh; okular` de manera que se seleccioni la instància correcta de l'Okular i les biblioteques.

## Paquets opcionals

Hi ha diversos paquets opcionals que es poden instal·lar per a tenir més característiques a l'Okular. Alguns podrien estar empaquetats per la vostra distribució, però altres potser no. Si voleu evitar problemes, quedeu-vos amb els paquets de la vostra distribució.

* Poppler (dorsal de PDF): Per a compilar el dorsal de PDF, cal la [biblioteca Poppler](http://poppler.freedesktop.org), amb la versió mínima requerida 0.24
* Libspectre: Per tal de compilar i usar aquest dorsal de PostScript (PS), cal la «libspectre» >= 0.2. Si la vostra distribució no l'empaqueta, o la versió empaquetada no és suficient, podeu baixar-la des d'[aquí](https://libspectre.freedesktop.org).
* DjVuLibre: Per a compilar el dorsal de DjVu, cal DjVuLibre >= 3.5.17. Com passa amb la Libspectre, la podeu aconseguir des de la vostra distribució o [aquí](http://djvulibre.djvuzone.org).
* libTIFF: Es requereix per a admetre TIFF/fax. Actualment no hi ha cap versió mínima requerida, així que qualsevol versió bastant recent de la biblioteca disponible a la vostra distribució hauria de funcionar. En cas de problemes, no dubteu a contactar amb els desenvolupadors de l'Okular.
* libCHM: Cal per a compilar el dorsal de CHM. Com passa amb la «libTIFF», no hi ha cap requisit de versió mínima.
* Libepub: Si cal admetre EPub, podeu instal·lar aquesta biblioteca a partir de la vostra distribució o des de [SourceForge](http://sourceforge.net/projects/ebook-tools).

## Desinstal·lació de l'Okular

L'Okular es pot desinstal·lar executant `make uninstall` al directori `build`. Això eliminarà l'aplicació del sistema. No eliminarà cap dada que hàgiu mirat, creat o modificat amb l'Okular.
