---
faq:
- answer: Pakiety na Ubuntu (a przez to na Kubuntu) Okulara są zbudowane bez obsługi
    tych dwóch formatów. Powód jest wyjaśniony w [tym](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    zgłoszeniu na Launchpadzie.
  question: Korzystając z Ubuntu nie mogę odczytać dokumentów CHM i ePub,pomimo posiadania
    zainstalowanych pakietów okular-extra-backends i libchm1. Dlaczego?
- answer: Ponieważ nie masz usługi mowy na swoim systemie, wgraj bibliotekę mowy Qt,
    a możliwość ta powinna stać się dla ciebie dostępna
  question: Dlaczego ustawienia mowy w menu Narzędzia są wyszarzone?
- answer: Wgraj pakiet poppler-data
  question: Pewne znaki nie są wyświetlane i przy włączeniu debugowania pewne wiersze
    pokazują 'Brakuje pakietu językowego dla xxx'
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Często zadawane pytania
---
