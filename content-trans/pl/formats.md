---
intro: Okular supports a wide variety of document formats and use cases.
layout: formats
menu:
  main:
    name: Format dokumentu
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Stan programów obsługujących formaty dokumentów
---
