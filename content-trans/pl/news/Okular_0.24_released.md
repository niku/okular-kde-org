---
date: 2015-12-16
title: Okular 0.24 wydany
---
Wersja 0.24 Okulara została wydana wraz z wydaniem Aplikacji do KDE 15.12. Wydanie to wprowadza pomniejsze poprawki błędów i nowe funkcje, pełny dziennik zmian możesz obejrzeć na  <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 jest zalecanym uaktualnieniem dla wszystkich go używających.
