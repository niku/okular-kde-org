---
date: 2014-04-16
title: Okular 0.19 wydany
---
Wersja 0.19 Okulara została wydana wraz z wydaniem Aplikacji do KDE 4.13. Wydanie to nabiera cech takich jak obsługa kart w interfejsie, wykorzystywanie parametru DPI ekranów, tak aby rozmiar strony pasował do rzeczywistego rozmiaru papieru, ulepszenia do struktury cofnij/powtórz oraz inne funkcje/ulepszenia. Okular 0.19 jest zalecanym uaktualnieniem dla wszystkich go używających.
