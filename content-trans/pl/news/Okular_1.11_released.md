---
date: 2020-08-13
title: Okular 1.11 wydany
---
Wydano wersję 1.11 Okulara. To wydanie wprowadza nowy interfejs opisywania dokumentów i wiele innych poprawek w różnych miejscach. Pełny dziennik zmian możesz obejrzeć na <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.11 wszystkim użytkownikom.
