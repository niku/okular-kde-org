---
date: 2018-04-19
title: Okular 1.4 wydany
---
Wersja 1.4 Okulara została wydana wraz z wydaniem Aplikacji do KDE 18.04. To wydanie wprowadza ulepszoną obsługę JavaScript w plikach PDF oraz obsługuje przerywanie wyświetlania PDF, co oznacza, że gdy masz złożony plik PDF i zmienisz jego powiększenie w chwili, gdy jest on wyświetlany, to zostanie ono przerwane natychmiastowo zamiast czekać, aż zostanie okno ukończone. Pełny dziennik zmian możesz obejrzeć na  <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.4 wszystkim użytkownikom.
