---
date: 2010-02-09
title: Okular 0.10 wydany
---
Wersja 0.10 programu Okular została opublikowana wraz z KDE SC 4.4. Oprócz ogólnej poprawy stabilności to wydanie wprowadza nowe/ulepszone wsparcie zarówno dla wyszukiwania wstecz, jak i wyszukiwania wprzód razem z łączeniem kodu źródłowego latex z odpowiadającymi miejscami w plikach dvi i pdf.
