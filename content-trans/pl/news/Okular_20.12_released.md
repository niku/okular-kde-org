---
date: 2020-12-10
title: Okular 20.12 wydany
---
Wydano wersję 20.12 Okulara. To wydanie usuwa małe błędy i daje nowe małe możliwości w wielu miejscach. Pełny dziennik zmian możesz zobaczyć na <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. Okular 20.12 jest zalecanym uaktualnieniem dla wszystkich używających tego programu.
