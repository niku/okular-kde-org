---
date: 2017-04-20
title: Okular 1.1 wydany
---
Wersja 1.1 Okulara została wydana wraz z wydaniem Aplikacji do KDE 17.04. To wydanie wprowadza możliwość zmiany rozmiaru przypisów, obsługę obliczania zawartości formularzy, usprawnienia do ekranów dotykowych oraz dużo więcej! Pełny dziennik zmian możesz obejrzeć na  <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.1 wszystkim użytkownikom.
