---
intro: Okular je na voljo v vnaprej pripravljenem paketu na številnih platformah.
  Na desni lahko preverite stanje paketa za vaš distribucijski sistem Linux ali nadaljujete
  z branjem informacij o drugih operacijskih sistemih
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular je že na voljo v večini distribucij Linuxa. Namestite ga lahko iz [KDE
    Software Center](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Flatpak logo
  name: Flatpak
  text: Najnovejšo različico [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)
    lahko namestite s strani Flathub. Eksperimentalne Flatpake z nočnimi gradnjami
    Okularja lahko [namestite iz repozitorija KDE Flatpak](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications).
- image: /images/ark.svg
  image_alt: Ark logo
  name: Izdaje izvorne kode
  text: Okular se redno izdaja kot del aplikacij KDE. Če ga želite izgraditi iz izvorne
    kode, lahko preverite razdelek [Build It](/build-it).
- image: /images/windows.svg
  image_alt: Windows logo
  name: Windows
  text: Za Windows si oglejte spletno stran <a href='https://community.kde.org/Windows'>KDE
    v programu Windows Initiative</a> za informacije o namestitvi programske opreme
    KDE v sistem Windows. Stabilna izdaja je na voljo v trgovini <a href='https://www.microsoft.com/store/apps/9N41MSQ1WNM8'>Microsoft
    Store</a>. Obstajajo tudi <a href='https://binary-factory.kde.org/job/Okular_Nightly_win64/'>poskusni
    nočni strojni prevodi</a>. Preizkusite in pošljite popravke.
sassFiles:
- /sass/download.scss
title: Prenos
---
## Namestitev

Če želite namestiti Okular sledite navodilom platforme, ki jo uporabljate za namestitev programske opreme. Večina platform ima grafični grafični program za nameščanje, ki se lahko uporablja za namestitev programskih paketov. V mnogih primerih se samodejno odpre, ko kliknete na prenos ali povezavo za namestitev na vašo platformo.

Če uporabljate Linux, je Okular morda že vnaprej nameščen v vašem sistemu kot del privzete izbire programov. Če ni, ga lahko namestite s orodjem za upravljanje paketov iz distribucije Linuxa. Za podrobnosti si oglejte njegovo dokumentacijo.

V mnogih primerih boste imeli priložnost namestiti Okular na modularni način, tako da se lahko odločite za namestitev podpore za posebne formate ločeno ali namestite prevode uporabniškega vmesnika. Module izberite module glede na vaše potrebe in preference.

## Odstranjevanje

Če želite odstraniti Okular, sledite navodilom orodja za upravljanje paketov, ki ste jo uporabili za namestitev Okularja. To bo odstranilo aplikacijo Okular. Ne bo se dotikal podatkov, ki ste jih videli, ustvarili ali spremenili z Okularjem.
