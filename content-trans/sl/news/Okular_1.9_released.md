---
date: 2019-12-12
title: Izšel je Okular 1.9
---
Izšla je različica 1.9 Okular. Ta izdaja med različnimi popravki in majhnimi funkcijami uvaja podporo cb7 za arhive ComicBook, izboljšano podporo JavaScripta za datoteke PDF. Celoten dnevnik sprememb lahko preverite na <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Okular 1.9 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
