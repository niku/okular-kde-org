---
date: 2016-12-15
title: Izšel je Okular 1.0
---
Različica 1.0 programa Okular je bila izdana skupaj s KDE Applications 16.12. Ta izdaja zdaj temelji na KDE Frameworks 5, lahko preverite celoten dnevnik sprememb na <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Okular 1.0 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
