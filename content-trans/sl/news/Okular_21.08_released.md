---
date: 2021-08-12
title: Izšel je Okular 21.08
---
Izšla je različica Okular za sistem 21.08. Ta izdaja vsebuje različne manjše popravke in možnosti. Lahko preverite celotni dnevnik sprememb na <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https://kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. Okular 21.08 je priporočena posodobitev za vse, ki uporabljajo Okular.
