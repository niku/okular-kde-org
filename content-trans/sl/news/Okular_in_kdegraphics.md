---
date: 2007-04-04
title: Okular v kdegraphics
---
Ekipa Okularja s ponosom objavlja, da je Okular zdaj del modula kdegraphics. Naslednja verzija Okularja bo dobavljena skupaj s KDE 4.0.
