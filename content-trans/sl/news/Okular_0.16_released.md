---
date: 2013-02-06
title: Izšel je Okular 0.16
---
Različica Okular 0.16 je bila izdana skupaj s KDE Applications 4.10. Ta izdaja uvaja nove funkcije, kot je možnost povečave na višje ravni v dokumentih PDF, aktivni pregledovalnik za tablične naprave, več podpore za filme PDF, pregledovanje po izbiri in izboljšave urejanja zaznamkov. Okular 0.16 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
