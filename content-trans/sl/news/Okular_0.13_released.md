---
date: 2011-07-27
title: Izšel je Okular 0.13
---
Različica 0.13 programa Okular je bila izdana skupaj z izdajo programov KDE 4.7. Ta izdaja predstavlja majhne popravke in zmožnosti in je priporočena posodobitev za vse, ki uporabljajo Okular.
