---
date: 2008-10-03
title: Izšel je Okular 0.7.2
---
Druga izdaja vzdrževanja serije KDE 4.1 vključuje Okular 0.7.2. Vključuje nekaj manjših popravkov v zaledjih TIFF in Comicbook in spremembo na "Prilagodi širini" kot privzeto raven povečave. Opis vseh odpravljenih težave si lahko preberete na <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
