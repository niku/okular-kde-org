---
intro: Okular podpira široko izbor formatov dokumentov in primerov uporabe.
layout: formats
menu:
  main:
    name: Format dokumenta
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Status upravljavcev formatov dokumenta
---
