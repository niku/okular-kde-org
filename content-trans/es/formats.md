---
intro: Okular reconoce una amplia variedad de formatos de documento y casos de uso.
layout: formats
menu:
  main:
    name: Formato de documentos
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Estado de los gestores de formatos de documentos
---
