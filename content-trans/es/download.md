---
intro: Okular está disponible como paquete precompilado en una amplia variedad de
  plataformas. Puede comprobar el estado del paquete para su distribución Linux a
  la derecha o seguir leyendo para obtener información sobre otros sistemas operativos.
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular ya está disponible en la mayoría de las distribuciones Linux. Puede
    instalarlo desde el [Centro de software de KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logotipo de Flatpak
  name: Flatpak
  text: Puede instalar el [Flatpak de Okular](https://flathub.org/apps/details/org.kde.okular)
    más reciente desde Flathub. Puede instalar flatpaks experimentales con compilaciones
    diarias de Okular desde el [repositorio Flatpak de KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications).
- image: /images/ark.svg
  image_alt: Logotipo de Ark
  name: Fuentes de lanzamientos
  text: Okular se publica regularmente como parte de KDE Gear. Si desea compilarlo
    a partir de su código fuente, visite la [sección de compilación](/build-it).
- image: /images/windows.svg
  image_alt: Logotipo de Windows
  name: Windows
  text: Consulte la [iniciativa de KDE en Windows](https://community.kde.org/Windows)
    para obtener información sobre cómo instalar software de KDE en Windows. La versión
    estable está disponible en la [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    También existen [versiones experimentales diarias](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    para las que se agradecen pruebas e informes de errores.
sassFiles:
- /sass/download.scss
title: Descarga
---
## Instalación

Para instalar Okular, siga las instrucciones de la plataforma que esté usando para instalar software. La mayoría de las plataformas disponen de un instalador gráfico que se puede usar para instalar paquetes de software. En muchos casos se abrirá automáticamente al pulsar el enlace de descarga o de instalación para su plataforma.

Si está usando Linux, es posible que Okular ya esté preinstalado en su sistema como parte de la selección predeterminada. En caso contrario, puede instalarlo con la herramienta de gestión de paquetes de su distribución Linux. Consulte su documentación para más detalles.

En muchos casos tendrá la oportunidad de instalar Okular de una forma modular, en la que podrá decidir si se instala el soporte para determinados formatos de forma separada o si se instalan las traducciones de la interfaz de usuario. Elija los módulos según sus necesidades y preferencias personales.

## Desinstalación

Para desinstalar Okular, siga las instrucciones de la herramienta de gestión de paquetes que haya usado para instalar Okular. Solo se eliminará la aplicación Okular, pero no se verá afectado ningún archivo que haya visualizado, creado o modificado con Okular.
