---
date: 2009-04-02
title: Okular 0.8.2 publicado
---
El segundo lanzamiento de la serie KDE 4.2 incluye Okular 0.8.2. Incluye soporte ampliamente mejorado para búsqueda inversa en documentos DVI y pdfsync, y algunas mejoras y correcciones menores en el modo de presentación.Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
