---
date: 2018-04-19
title: Okular 1.4 publicado
---
La versión 1.4 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 18.04. Esta versión introduce mejoras en el uso de JavaScript en archivos PDF y permite cancelar la visualización en PDF, lo que significa que si tiene un archivo PDF complejo y modifica la ampliación mientras se está generando su visualización, el proceso se interrumpirá inmediatamente en lugar de tener que esperar a que se termine de generar. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. Okular 1.4 es una actualización recomendada para todos los usuarios de Okular.
