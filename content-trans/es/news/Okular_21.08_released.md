---
date: 2021-08-12
title: Okular 21.08 publicado
---
La versión 21.08 de Okular ha sido publicada. Esta versión contiene varias correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https:/kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. Okular 21.08 es una actualización recomendada para todos los usuarios de Okular.
