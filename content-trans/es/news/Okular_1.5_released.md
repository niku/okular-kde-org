---
date: 2018-08-16
title: Okular 1.5 publicado
---
La versión 1.5 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 18.08. Esta versión introduce mejoras en formularios entre otras correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Okular 1.5 es una actualización recomendada para todos los usuarios de Okular.
