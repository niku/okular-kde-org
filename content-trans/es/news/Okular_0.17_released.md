---
date: 2013-08-16
title: Okular 0.17 publicado
---
La versión 0.17 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 4.11. Esta versión incluye nuevas funcionalidades como la implementación de deshacer/rehacer para formularios y anotaciones y las herramientas de revisión configurables. Okular 0.17 es una actualización recomendada para todos los usuarios de Okular.
