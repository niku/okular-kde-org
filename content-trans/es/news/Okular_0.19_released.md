---
date: 2014-04-16
title: Okular 0.19 publicado
---
La versión 0.19 de Okular ha sido publicada junto al lanzamiento de las Aplicaciones de KDE 4.13. Esta versión incluye nuevas funcionalidades como el uso de pestañas en la interfaz, el uso de la resolución de la pantalla para que el tamaño de la página se corresponda con el tamaño real del papel, mejoras en la infraestructura de deshacer y rehacer, y alguna otra funcionalidad o. refinamiento. Okular 0.19 es una actualización recomendada para todos los usuarios de Okular.
