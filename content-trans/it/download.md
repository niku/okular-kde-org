---
intro: Okular è disponibile come pacchetto precompilato per un'ampia gamma di sistemi.
  Sulla destra puoi verificare lo stato dei pacchetti per la tua distribuzione Linux,
  oppure continuare a leggere le informazioni sugli altri sistemi operativi
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular è già disponibile nella maggior parte delle distribuzioni Linux. Puoi
    installarlo dal [KDE Software Center](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logo di Flatpak
  name: Flatpak
  text: Puoi installare l'ultima versione [Flatpak di Okular](https://flathub.org/apps/details/org.kde.okular)
    da Flathub. I flatpak sperimentali con build notturne di Okular possono essere
    [installati dal repository Flatpak di KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Logo di Ark
  name: Sorgenti di rilascio
  text: Okular è rilasciato regolarmente come parte di KDE Gear. Se vuoi generarlo
    dal sorgente, puoi consultare il [paragrafo Compilalo](/build-it).
- image: /images/windows.svg
  image_alt: Logo di Windows
  name: Windows
  text: Per informazioni su come installare software KDE su Windows, dai un'occhiata
    alla [Iniziativa KDE su Windows](https://community.kde.org/Windows). Il rilascio
    stabile è disponibile nel [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    Esistono anche [build notturne sperimentali](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    per le quali sono ben accetti test e segnalazioni di errori.
sassFiles:
- /sass/download.scss
title: Scarica
---
## Installazione

Per installare Okular segui le istruzioni di installazione relative alla piattaforma che stai utilizzando. La maggior parte di esse possiede un installatore grafico che ti permette di installare pacchetti software. In molti casi esso sarà automaticamente aperto quando fai clic sul collegamento per lo scaricamento o l'installazione relativo alla tua piattaforma.

Se usi Linux, Okular potrebbe essere già preinstallato sul sistema come parte di una selezione predefinita. Se così non fosse, puoi installarlo tramite il gestore di pacchetti della tua distribuzione. Per i dettagli consulta la sua documentazione relativa.

In molti casi avrai l'opportunità di installare Okular in maniera modulare, dunque potrai decidere di installare separatamente il supporto per specifici formati, oppure installare le traduzioni dell'interfaccia utente. Scegli i moduli in base alle tue necessità e preferenze.

## Disinstallazione

Per disinstallare Okular segui le istruzioni del gestore dei pacchetti che hai utilizzato per installare il programma. L'applicazione verrà rimossa dal sistema. I dati utente che hai visualizzato, creato o modificato con Okular non verranno toccati.
