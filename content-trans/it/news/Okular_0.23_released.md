---
date: 2015-08-19
title: Rilasciato Okular 0.23
---
La versione 0.23 di Okular è stato rilasciata insieme con le applicazioni di KDE 15.08. Questo rilascio introduce il supporto alla transizione Dissolvenza in modalità presentazione e alcune correzioni di errori riguardanti le annotazioni e la riproduzione video. È un aggiornamento consigliato per chiunque utilizzi Okular.
