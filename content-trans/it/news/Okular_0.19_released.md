---
date: 2014-04-16
title: Rilasciato Okular 0.19
---
La versione 0.19 di Okular è stato rilasciata insieme con le applicazioni di KDE 4.13. Questo rilascio introduce nuove funzionalità come il supporto delle schede nell'interfaccia, l'uso della schermata DPI in modo che corrisponda alla reale dimensione della pagina, miglioramenti alle funzioni di Annulla/Rifai e altre ancora. È un aggiornamento consigliato per chiunque utilizzi Okular.
