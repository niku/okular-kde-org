---
date: 2007-07-10
title: Materiale in linea di presentazione di Okular all'aKademy 2007
---
La presentazione su Okular di Pino Toscano all'<a href="http://akademy2007.kde.org">aKademy 2007</a> è ora in linea. Sono disponibili sia le <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">diapositive</a>, sia il <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">video</a>.
