---
date: 2014-12-17
title: Rilasciato Okular 0.21
---
La versione 0.21 di Okular è stato rilasciata insieme con le applicazioni di KDE 14.12. Questo rilascio introduce nuove funzionalità come la ricerca inversa sulla funzione synctex di latex in dvi e diverse correzioni di errori. È un aggiornamento consigliato per chiunque utilizzi Okular.
