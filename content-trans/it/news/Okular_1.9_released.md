---
date: 2019-12-12
title: Rilasciato Okular 1.9
---
È stata rilasciata la versione 1.9 di Okular. Questo rilascio introduce supporto cb7 per gli archivi ComicBook, supporto JavaScript migliorato per i file PDF, varie correzioni e piccole funzionalità. Puoi verificare le novità di versione all'indirizzo <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
