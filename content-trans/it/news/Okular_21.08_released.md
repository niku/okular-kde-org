---
date: 2021-08-12
title: Rilasciato Okular 21.08
---
È stata rilasciata la versione 21.08 di Okular. Questo rilascio contiene  varie correzioni minori e aggiunte di funzionalità dappertutto. Puoi verificare le novità di versione all'indirizzo <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https://kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
