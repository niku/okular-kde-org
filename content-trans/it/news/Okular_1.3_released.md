---
date: 2017-12-14
title: Rilasciato Okular 1.3
---
La versione 1.3 di Okular è stato rilasciata insieme con le applicazioni di KDE 17.12. Questo rilascio introduce modifiche al salvataggio delle annotazioni e ai lavori sui dati di formulario, supporta parzialmente gli aggiornamenti di resa per i file per i quali sono richiesti lunghi tempi di elaborazione, rende i collegamenti di testo attivi nella modalità di selezione testo, aggiunge un'opzione Condividi nel menu File, aggiunge il supporto a Markdown e risolve alcuni problemi concernenti il supporto a HiDPI. Puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
