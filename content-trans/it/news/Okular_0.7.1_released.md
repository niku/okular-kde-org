---
date: 2008-09-03
title: Rilasciato Okular 0.7.1
---
Il primo rilascio di manutenzione della serie KDE 4.1 comprende Okular 0.7.1. Include alcune correzioni dei crash, oltre ad altre correzioni minori. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
