---
date: 2013-02-06
title: Rilasciato Okular 0.16
---
La versione 0.16 di Okular è stato rilasciata insieme con le applicazioni di KDE 4.10. Questo rilascio introduce nuove funzionalità, come la capacità di ingrandimento maggiore nei documenti PDF, un visualizzatore basato su Active per i tablet, maggiore supporto per i filmati PDF, l'area di visualizzazione segue la selezione e sono stati fatti miglioramenti nella modifica delle annotazioni. Okular 0.16 è un aggiornamento consigliato per chiunque utilizzi Okular.
