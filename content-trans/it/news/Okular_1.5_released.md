---
date: 2018-08-16
title: Rilasciato Okular 1.5
---
La versione 1.5 di Okular è stato rilasciata insieme con KDE Applications 18.08. Questo rilascio introduce miglioramenti ai moduli, varie correzioni e piccole funzionalità. Puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
