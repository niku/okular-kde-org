---
intro: Okular supporta un'ampia varietà di formati di documento e casi d'uso.
layout: formats
menu:
  main:
    name: Formato dei documenti
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Stato dei gestori di formati del documento
---
