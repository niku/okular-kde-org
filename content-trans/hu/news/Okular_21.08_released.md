---
date: 2021-08-12
title: Megjelent az Okular 21.08
---
The 21.08 version of Okular has been released. This release contains various minor fixes and features all over. You can check the full changelog at <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https://kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. Okular 21.08 is a recommended update for everyone using Okular.
