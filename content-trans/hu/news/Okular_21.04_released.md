---
date: 2021-04-22
title: Megjelent az Okular 21.04
---
The 21.04 version of Okular has been released. This release introduces Digital Signing of PDF files and various minor fixes and features all over. You can check the full changelog at <a href='https://kde.org/announcements/changelogs/releases/21.04.0/#okular'>https://kde.org/announcements/changelogs/releases/21.04.0/#okular</a>. Okular 21.04 is a recommended update for everyone using Okular.
