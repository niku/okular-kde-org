---
date: 2013-08-16
title: Megjelent az Okular 0.17
---
Az Okular 0.17-es kiadása a KDE Alkalmazások 4.11-gyel együtt jelent meg. Ez a kiadás új szolgáltatásokat mutat be, mint például visszavonás/újra támogatás az űrlapokhoz és magyarázatokhoz, illetve a beállítható áttekintő eszközöket. Az Okular 0.17 javasolt frissítés minden Okular felhasználónak.
