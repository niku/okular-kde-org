---
date: 2012-10-10
title: Okular fórumok
---
Mostanra van egy alfórumunk a KDE közösségi fórumokon belül. Megtalálható a <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a> címen.
