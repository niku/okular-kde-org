---
intro: Okular est disponible comme un paquet pré compilé sur une large variété de
  plate-formes. Vous pouvez vérifier l'état du paquet pour votre distribution Linux
  sur la droite ou poursuivre la lecture des informations sur d'autres systèmes d'exploitation.
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular est déjà disponible sur la plupart des distributions Linux. Vous pouvez
    l'installer à partir du [Centre des applications de KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logo de Flatpak
  name: Flatpak
  text: Vous pouvez installer la dernière version par [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)
    à partir de Flathub. Des Flatpaks expérimentaux avec les compilations de développement
    peuvent être [installées à partir du dépôt Flatpak de KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Logo de Ark
  name: Publier les sources
  text: Okular est publié de façon fréquente comme partie des applications de KDE
    Gear. Si vous voulez le compiler à partir des sources, vous pouvez vérifier la
    [section « Compiler le » ](/build-it).
- image: /images/windows.svg
  image_alt: Logo de Windows
  name: Windows
  text: Veuillez regarder [l'initiative KDE sous Windows](https://community.kde.org/Windows)
    pour des informations sur comment installer des logiciels de KDE sous Windows.
    La version stable est disponible sous la [boutique Microsoft](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    Il y a aussi des [compilations de versions expérimentales](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    pour lesquelles des rapports de tests et de bogues seront les bienvenus.
sassFiles:
- /sass/download.scss
title: Téléchargement
---
## Installation

Pour installer Okular, veuillez suivre les instructions de la plate-forme que vous utilisez sur la façon d'installer des logiciels. La plupart des plateformes disposent d'un programme d'installation graphique pouvant être utilisé pour l'installation de logiciels. Dans de nombreux cas, il se lancera automatiquement suite à un clic sur le lien de téléchargement ou d'installation pour votre plate-forme.

Si vous utilisez Linux, Okular est peut-être déjà préinstallé sur votre système comme logiciel sélectionné par défaut. Sinon, vous pouvez l'installer avec l'outil de gestion des paquets de votre distribution Linux. Veuillez consulter sa documentation pour plus de détails.

Dans de nombreux cas, vous aurez la possibilité d'installer Okular de manière modulaire. Vous pourrez ainsi décider d'installer séparément la prise en charge de formats spécifiques ou d'installer des traductions pour l'interface utilisateur. Veuillez choisir les modules en fonction de vos besoins et de vos préférences.

## Désinstallation

Pour désinstaller Okular, veuillez suivre les instructions de l'outil de gestion des paquets que vous avez utilisé son installation. Il supprimera alors l'application Okular. Elle ne touchera pas aux données que vous avez consultées, créées ou modifiées avec Okular.
