---
date: 2018-12-13
title: Publication de la version 1.6 d'Okular
---
La version 1.6 d'Okular a été livrée avec la version 18.12 des applications KDE. Cette version intègre les nouvelles annotations « machine à écrire » ainsi que des corrections et fonctionnalités mineures. Vous pouvez consulter la liste complète des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. La version 1.6 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
