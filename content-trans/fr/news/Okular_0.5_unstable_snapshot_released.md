---
date: 2006-08-27
title: Publication de la version de développement instable 0.5 d'Okular
---
L'équipe d'Okular est fière de vous annoncer la livraison d'une pré version d'Okular correspondant à la <a href="http://dot.kde.org/1155935483/"> pré version KDE 4 « Krash »</a>. Cette pré version n'est pas encore tout à fait fonctionnelle puisqu'il reste beaucoup de choses à affiner et à terminer. Mais, vous êtes libre de la tester et de de nous fournir autant de retours que vous pouvez. Vous pouvez trouver le paquet de la pré version à <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2</a>. Vous pouvez aussi consulter la page de<a href="download.php">téléchargement</a> pour être sûr que vous possédez toutes les bibliothèques nécessaires.
