---
date: 2021-08-12
title: Publication de la version 21.08 d'Okular
---
La version 21.08 d'Okular a été livrée. Cette version intègre de nombreuses corrections mineures et enfin certaines fonctionnalités. Vous pouvez consulter la liste complète des changements à cette adresse : <a href='https://kde.org/announcements/changelogs/releases/21.08.0/#okular'>https://kde.org/announcements/changelogs/releases/21.04.0/#okular</a>. La version 21.08 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
