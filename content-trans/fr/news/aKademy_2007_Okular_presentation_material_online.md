---
date: 2007-07-10
title: Matériel de présentation concernant Okular et mis en ligne suite à « Akademy
  2007 »
---
Le discours sur Okular réalisé par Pino Toscano à <a href="http://akademy2007.kde.org">aKademy 2007</a> est maintenant en ligne. il y a, à la fois, des <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">diapositives</a> et <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">vidéos</a> disponibles.
