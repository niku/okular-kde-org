---
date: 2009-05-06
title: Publication de la version 0.8.3 d'Okular
---
La version 0.8.3 d'Okular a été livrée avec la troisième version de maintenance de KDE 4.2. Elle ne fournit que peu de nouveautés pour Okular. Le seul changement significatif est le traitement plus sûr de la génération des pages d'images de document « XPS ». Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
