---
date: 2017-12-14
title: Publication de la version 1.3 d'Okular
---
La version 1.3 d'Okular a été livrée avec les applications fournies avec la version 17.12 de KDE. Cette version intègre des changements dans l'enregistrement des données des formulaires et des annotations, une prise en charge du rendu partiel pour les fichiers mettant beaucoup de temps à s'afficher, rend les liens texte interactifs en mode sélection de texte, ajoute une option « Partager » dans le menu « Fichier », prend en charge Markdown et corrige quelques problèmes relatifs à la prise en charge de HiDPI. Vous pouvez consulter la liste des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. La version 1.3 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
