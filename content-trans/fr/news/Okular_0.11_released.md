---
date: 2010-08-10
title: Publication de la version 0.11 d'Okular
---
La version 0.11 d'Okular a été livrée avec les applications fournies avec la version 4.5 de KDE. Cette version intègre des corrections et des fonctionnalités mineures. Elle est une mise à jour recommandée pour toute personne utilisant Okular. 
