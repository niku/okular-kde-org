---
date: 2011-07-27
title: Publication de la version 0.13 d'Okular
---
La version 0.13 d'Okular a été livrée avec les applications fournies avec la version 4.7 de KDE. Cette version intègre des corrections et des fonctionnalités mineures. Elle est une mise à jour recommandée pour toute personne utilisant Okular. 
