---
intro: Okular prend en charge une grande variété de formats de documents et de cas
  d'utilisations.
layout: formats
menu:
  main:
    name: Format du document
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: État des gestionnaires de formats de documents
---
