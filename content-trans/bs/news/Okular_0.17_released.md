---
date: 2013-08-16
title: Okular 0.17 pušten
---
Verzija 0.17 Okulara je puštena zajedno sa KDE 4.11 aplikacijom. Ovo izdanje predstavlja nove mogućnosti poput poništi/ponovo uradi podrške za obrasce i bilješke te podesivi pregled alata. Okular 0.17 je preporučena nadogradnja za sve korisnike Okulara.
