---
date: 2010-08-10
title: Okular 0.11 izdan
---
Verzija 0.11 Okulara je puštena zajedno sa KDE 4.5 aplikacijom. Ovo izdanje predstavlja mala poboljšanja i nove mogućnosti i  preporučena je nadogradnja za sve korisnike Okulara.
