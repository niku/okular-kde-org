---
date: 2008-10-03
title: Κυκλοφορία της έκδοσης 0.7.2 του Okular
---
Η δεύτερη έκδοση συντήρησης της σειράς 4.1 του KDE περιλαμβάνει το Okular 0.7.2 με κάποιες ήσσονος σημασίας διορθώσεις στα συστήματα υποστήριξης των TIFF και Comicbook και την αλλαγή σε "Προσαρμογή στο πλάτος" ως το προεπιλεγμένο επίπεδο διαστάσεων για ανάγνωση. Μπορείτε να διαβάσετε για όλα τα διορθωμένα προβλήματα στο <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
