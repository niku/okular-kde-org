---
date: 2009-05-06
title: Κυκλοφορία της έκδοσης 0.8.3 του Okular
---
Η τρίτη έκδοση συντήρησης της σειράς 4.2 του KDE περιλαμβάνει το Okular 0.8.3. Δεν φέρνει κάτι νέο για το Okular, η μόνη αλλαγή αφορά την αυξημένη ασφάλεια νημάτων εκτέλεσης κατά την παραγωγή σελιδο-εικόνων για έγγραφα XPS. Μπορείτε να διαβάσετε για όλα τα διορθωμένα προβλήματα στο <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
