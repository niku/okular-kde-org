---
date: 2018-04-19
title: Κυκλοφορία της έκδοσης 1.4 του Okular
---
Η έκδοση 1.4 του Okular κυκλοφόρησε μαζί με την έκδοση 18.04 του πακέτου KDE Applications. Η έκδοση αυτή εισάγει βελτιωμένη υποστήριξη για JavaScript σε PDF αρχεία και υποστηρίζει ακύρωση της διαδικασίας αποτύπωσης σε PDF, που σημαίνει ότι αν έχετε ένα πολύπλοκο αρχείο PDF και αλλάξετε την εστίαση κατα την αποτύπωση, θα γίνει άμεση ακύρωση της αποτύπωσης αντί της αναμονής για την ολοκλήρωσή της. Μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.4.
