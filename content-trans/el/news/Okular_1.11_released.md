---
date: 2020-08-13
title: Κυκλοφορία της έκδοσης 1.11 του Okular
---
Η έκδοση 1.11 του Okular κυκλοφόρησε. Η έκδοση αυτή εισάγει μια νέα διεπαφή σχολιασμούψκαι διάφορες μικρές διορθώσεις και χαρακτηριστικά. Μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.11.
