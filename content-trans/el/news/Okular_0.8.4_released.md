---
date: 2009-06-03
title: Κυκλοφορία της έκδοσης 0.8.4 του Okular
---
Η τέταρτη έκδοση συντήρησης της σειράς 4.2 του KDE περιλαμβάνει το Okular 0.8.4 με κάποιες διορθώσεις για έγγραφα τύπου OpenDocument Text, κάποιες διορθώσεις κατάρρευσης και ορισμένες διορθώσεις σφαλμάτων ήσσονος σημασίας στη διεπαφή. Μπορείτε να διαβάσετε για όλα τα διορθωμένα προβλήματα στο <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
