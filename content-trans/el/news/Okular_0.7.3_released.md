---
date: 2008-11-04
title: Κυκλοφορία της έκδοσης 0.7.3 του Okular
---
Η τρίτη έκδοση συντήρησης της σειράς 4.1 του KDE περιλαμβάνει το Okular 0.7.3 με κάποιες ήσσονος σημασίας διορθώσεις στη διεπαφή και στην αναζήτηση κειμένου. Μπορείτε να διαβάσετε για όλα τα διορθωμένα προβλήματα στο <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
