---
date: 2017-08-17
title: Κυκλοφορία της έκδοσης 1.2 του Okular
---
Η έκδοση 1.2 του Okular κυκλοφόρησε μαζί με την έκδοση 17.08 του πακέτου KDE Applications. Η έκδοση αυτή εισάγει μικρές διορθώσεις και βελτιώσεις. Μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.2.
