---
menu:
  main:
    name: Κατασκεύασέ το
    parent: about
    weight: 2
title: Μεταγλώττιση του Okular από τον πηγαίο κώδικα στο Linux
---
<span style="background-color:#e8f4fa">Αν αναζητάτε τα προ-μεταγλωττισμένα πακέτα, επισκεφθείτε τη [σελίδα λήψης](/download/). Μπορείτε να ελέγξετε την κατάσταση του πακεταρίσματος [εδώ](https://repology.org/project/okular/versions)</span>

Αν θέλετε να μεταγλωττίσετε το Okular, χρειάζεστε ένα περιβάλλον μεταγλώττισης, το οποίο, γενικά, σας το παρέχει η διανιμή σας. Σε περίπτωση που θέλετε να μεταγλωττίσετε την αναπτυσσόμενη έκδοση του Okular, σας παραπέμπουμε στην Κατασκευή από την πηγή στο Wiki της Κοινότητας του KDE.

Μπορείτε να κάνετε λήψη και να μεταγλωττίσετε το Okular με αυτόν τον τρόπο:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Αν εγκαταστήσατε το Okular σε διαφορετική διαδρομή από εκείνη του καταλόγου εγκατάστασης του συστήματος, ίσως χρειαστεί να εκτελέσετε `source build/prefix.sh; okular` ώστε να χρησιμοποιηθεί η σωστή διεργασία του Okular και οι σωστές βιβλιοθήκες.

## Προαιρετικά πακέτα

Υπάρχουν κάποια προαιρετικά πακέτα που μπορείτε να εγκαταστήσετε για να έχει το Okular αυξημένη λειτουργικότητα. Ορισμένα ίσως να έχουν ήδη πακεταριστεί γα τη διανομή σας, και άλλα όχι. Αν θέλετε να αποφύγετε προβλήματα, παραμείνετε στα πακέτα που υποστηρίζει η διανομή σας

* Poppler (σύστημα υποστήριξης PDF): Για να μεταγλωττίσετε το σύστημα υποστήριξης PDF, χρειάζεστε τη [βιβλιοθήκη Poppler] (http://poppler.freedesktop.org), για την οποία η ελάχιστη απαιτούμενη έκδοση είναι η 0.24
* Libspectre: In order to compile and use this PostScipt (PS) backend, you need libspectre >= 0.2. If your distro does not package it, or the packaged version is not enough, you can download it [here](http://libspectre.freedesktop.org)
* DjVuLibre: To compile the DjVu backend, you need DjVuLibre >= 3.5.17. As with Libspectre, you can get it from your distro or [here](http://djvulibre.djvuzone.org).
* libTIFF: This is required for TIFF/fax support. Currently there is no minimum required version, so any quite recent version of the library available from your distro should work. In case of trouble, do not hesitate to contact the Okular developers.
* libCHM: This is needed to compile the CHM backend. As with libTIFF, there is no minimum version requirement
* Libepub: If you need EPub support, you can install this library from your distro or from [sourceforge](http://sourceforge.net/projects/ebook-tools).

## Uninstalling Okular

You can uninstall Okular by running `make uninstall` in your `build` dir. This will remove the application from your system. It will not remove any user data you have viewed, created, or modified with Okular.
