---
intro: Okular is beschikbaar als een voorgecompileerd pakket op een brede reeks platforms.
  U kunt de pakketstatus voor uw Linux distributie rechts controleren of blijven lezen
  voor informatie op andere besturingssystemen
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular is al beschikbaar op de meeste Linux distributies. U kunt het installeren
    uit het [KDE Software centrum](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Flatpak-logo
  name: Flatpak
  text: U kunt de laatste [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)
    uit Flathub installeren. Experimentele Flatpaks met nachtelijke builds van Okular
    kunnen [geïnstalleerd worden uit de KDE Flatpak opslagruimte](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Ark-logo
  name: Uitgavebronnen
  text: Okular wordt regelmatig uitgegeven als onderdeel van KDE Gear. Als u vanaf
    de broncode wilt bouwen dan kunt u de [sectie bouw het](/build-it) bekijken.
- image: /images/windows.svg
  image_alt: Windows-logo
  name: Windows
  text: Kijk op de webpagina [KDE on Windows initiative](https://community.kde.org/Windows)
    voor informatie over hoe KDE Software onder Windows te installeren. De stabiele
    uitgave is beschikbaar in de [Microsoft Store](https://www.microsoft.com/store/apps/9N41MSQ1WNM8).
    Er zijn ook [experimentele nachtelijke builds](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    waarvoor testen en rapporten voor bugs welkom zijn.
sassFiles:
- /sass/download.scss
title: Downloaden
---
## Installatie

Om Okular te installeren volg de instructies van het platform dat u gebruikt voor hoe software te installeren. De meeste platforms hebben een grafisch installatieprogramma die gebruikt kan worden om softwarepakketten te installeren. In veel gevallen zal het automatisch geopend worden door te klikken op de koppeling voor downloaden of installeren voor uw platform.

Als u Linux gebruikt, zou Okular al vooraf geïnstalleerd kunnen zijn op uw systeem als onderdeel van een standaard selectie. Zo niet, dan kunt u het installeren met het hulpmiddel voor pakketbeheer van uw Linux distributie. Zie zijn documentatie voor details.

In vele gevallens zult u de mogelijkheid hebben om Okular op een modulaire manier te installeren, zodat u kunt beslissen om ondersteuning voor specifieke formaten separaat te installeren of vertalingen voor het gebruikersinterface te installeren. Kies de modulen volgens wat u nodig heeft en volgens uw voorkeur.

## Installatie ongedaan maken

Om de installatie van Okular ongedaan te maken volgt u de instructies van het hulpmiddel voor pakketbeheer dat u hebt gebruikt om Okular te installeren. Dit zal de toepassing uit uw systeem verwijderen. Het zal geen gebruikersgegevens, die u hebt bekeken, aangemaakt of gewijzigd met Okular, verwijderen.
