---
intro: Okular ondersteunt een brede variëteit van documentformaten en soorten van
  gebruik.
layout: formats
menu:
  main:
    name: Documentformaat
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Status van documentformaatbehandelaars
---
