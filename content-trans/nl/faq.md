---
faq:
- answer: Ubuntu (dus ook Kubuntu) pakketten van Okular zijn gecompileerd zonder de
    ondersteuning voor deze twee formaten. De reden is in [dit](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    Launchpad rapport uitgelegd.
  question: Bij gebruik van Ubuntu kan ik geen CHM- en EPub-documenten lezen, zelfs
    als ik okular-extra-backends en libchm1 heb geïnstalleerd. Waarom?
- answer: Omdat u geen spraakservice hebt op uw systeem. Installeer de Qt-spraakbibliotheek
    en deze zal zijn ingeschakeld
  question: Waarom zijn de spraakopties in het menu Hulpmiddelen grijs?
- answer: Installeer het pakket poppler-data
  question: Sommige tekens worden niet weergegeven en bij inschakelen van debug melden
    sommige regels mention 'Taalpakket voor xxx ontbreekt'
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Veel voorkomende vragen
---
