---
date: 2007-01-31
title: Okular doet mee met "The Season of Usability project"
---
Het team van Okular is er trots op dat Okular is een van de toepassingen die is geselecteerd om mee te doen in het project <a href="http://www.openusability.org/season/0607/">Season of Usability</a>, dat wordt uitgevoerd door experts in bruikbaarheid op <a href="http://www.openusability.org">OpenUsability</a>. Van deze plaats verwelkomen we Sharad Baliyan in het team en danken Florian Graessle en Pino Toscano voor hun voortdurende werk aan het verbeteren van Okular.
