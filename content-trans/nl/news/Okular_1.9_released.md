---
date: 2019-12-12
title: Okular 1.9 vrijgegeven
---
De 1.9 versie van Okular is vrijgegeven. Deze vrijgave introduceert ondersteuning voor cb7 voor archieven met stripboeken, verbeterde ondersteuning voor JavaScript voor PDF-bestanden, naast verschillende andere reparaties en kleine functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Okular 1.9 wordt aanbevolen voor iedereen die Okular gebruikt.
