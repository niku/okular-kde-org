---
date: 2018-08-16
title: Okular 1.5 vrijgegeven
---
De 1.5 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 18.08. Deze vrijgave introduceert verbeteringen aan formulieren naast verschillende andere reparaties en functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Okular 1.5 wordt aanbevolen aan iedereen die Okular gebruikt.
