---
date: 2021-08-12
title: Okular 21.08 vrijgegeven
---
De versie 21.08 van Okular is vrijgegeven. Deze vrijgave bevat her en der verschillende kleine reparaties en functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https://kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. Okular 21.08 wordt aanbevolen voor iedereen die Okular gebruikt.
