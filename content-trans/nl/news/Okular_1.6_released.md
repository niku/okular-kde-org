---
date: 2018-12-13
title: Okular 1.6 vrijgegeven
---
De 1.6 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 18.12. Deze vrijgave introduceert het nieuwe annotatiehulpmiddel Typewriter naast verschillende andere reparaties en functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. Okular 1.6 wordt aanbevolen aan iedereen die Okular gebruikt.
