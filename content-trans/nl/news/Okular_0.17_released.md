---
date: 2013-08-16
title: Okular 0.17 vrijgegeven
---
De 0.17 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 4.11. Deze vrijgave introduceert nieuwe functies zoals ondersteuning van ongedaan maken / opnieuw doen voor formulieren en annotaties en instelbare hulpmiddelen voor herzien. Okular 0.17 wordt aanbevolen aan iedereen die Okular gebruikt.
