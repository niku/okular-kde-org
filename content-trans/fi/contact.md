---
menu:
  main:
    parent: about
    weight: 4
title: Yhteydenotot
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, KDE:n maskotti" style="height: 200px;"/>

Okularin kehitysryhmään saa yhteyden monin tavoin:

* Postituslista: Okularin kehityksen koordinoimiseen käytämme kde.orgissa [okular-devel-postituslistaa](https://mail.kde.org/mailman/listinfo/okular-devel). Sitä voi käyttää keskusteluun pääsovelluksen kehityksestä. Palautetta nykyisistä ja uusista taustajärjestelmistä arvostetaan.

* IRC: Yleiseen keskusteluun käytämme [FreeNode-verkossa](http://www.freenode.net) IRCiä [#okular](irc://irc.kde.org/#okular) ja [#kde-devel](irc://irc.kde.org/#kde-devel). Niistä voi löytää Okularin kehittäjiäkin.

* Matrix: Yllä mainittuun keskusteluun pääsee myös Matrix-verkosta [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Keskustelualue: Jos suosit keskustelualuetta, voit käyttää [Okular-foorumia](http://forum.kde.org/viewforum.php?f=251), joka on osa laajempaa [KDE-yhteisöfoorumia](http://forum.kde.org/).

* Ohjelmavirheet ja toiveet: Ohjelmavirheistä ja toiveista tulisi ilmoittaa (englanniksi) [KDE:n vianseurantajärjestelmään](https://bugs.kde.org/). Jos haluat auttaa, luettelon pahimmista virheistä löytää (täältä)[https://community.kde.org/Okular).
