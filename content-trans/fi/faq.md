---
faq:
- answer: Ubuntun (ja siten Kubuntunkin) Okular-paketit ovat käännetty ilman näiden
    tiedostomuotojen tukea. Syy selitetään [tässä](https://bugs.launchpad.net/kdegraphics/+bug/277077)
    Launchpad-tiedotteessa.
  question: Ubuntussa CHM- ja ePub-asiakirjoja ei voi lukea, vaikka okular-extra-backend-ja
    libchml-paketit on asennettu. Miksei?
- answer: Koska järjestelmässä ei ole KDE:n puhesynteesityökaluja. Asenna Qt Speech
    -kirjasto, jonka jälkeen niiden pitäisi tulla käyttöön
  question: Miksi työkaluvalikon Puhu-valinnat näkyvät harmaina?
- answer: Asenna poppler-data -paketti
  question: Joitakin merkkejä ei piirretty, ja kun virheenpaikannus otettiin käyttöön,
    joillakin riveillä mainitaan ”kieleltä xxx puuttuva kielipaketti”
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Usein kysytyt kysymykset
---
