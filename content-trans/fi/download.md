---
intro: Okularin saa esikäännettynä pakettina laajalle joukolle alustoja. Oikealta
  voi tarkistaa Linux-jakelun pakettien tilan, ja muista käyttöjärjestelmistä löytää
  tietoa tuonnempaa
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okularin saa jo useimpiin Linux-jakeluihin. Sen voi asentaa [KDE-ohjelmakeskuksesta](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Flatpakin logo
  name: Flatpak
  text: Flathubista voi asentaa viimeisimmän [Okular-flatpakin](https://flathub.org/apps/defailts/org.kde.okular).
    Kokeelliset flatpakit Okularin öisistä koosteista voi [asentaa KDE Flatpak -asennuslähteestä](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications).
- image: /images/ark.svg
  image_alt: Arkin logo
  name: Julkaisulähteet
  text: Okularia julkaistaan säännöllisesti osana KDE Geariä. Jos haluat koostaa sen
    lähteistä, vilkaise [Koostaminen-osiota](/build-it).
- image: /images/windows.svg
  image_alt: Windows-logo
  name: Windows
  text: Asentaaksesi KDE-ohjelmia Windowsiin vilkaise <a href='http://windows.kde.org/'>KDE
    on Windows Initiative</a> -sivustoa. Vakaa versio löytyy [Microsoft  Storesta](https://www.microsoft.com/store/apps/9n41msqwnm8).
    On myös [kokeellisia öisiä koosteita](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    joiden testaus ja virheistä ilmoittaminen on tervetullutta.
sassFiles:
- /sass/download.scss
title: Lataa
---
## Installation

To install Okular follow the instructions of the platform you are using for how to install software. Most platforms have a graphical installer which can be used to install software packages. In many cases it will automatically be opened when clicking the download or install link for your platform.

If you are using Linux, Okular might already be pre-installed on your system as part of a default selection. If not, you can install it with the package management tool of your Linux distribution. See its documentation for details.

In many cases you will have the opportunity to install Okular in a modular way, so you can decide to install support for specific formats separately or install translations for the user interface. Choose the modules according to your needs and preferences.

## Uninstallation

To uninstall Okular follow the instructions of the package management tool you have used to install Okular. This will remove the Okular application. It will not touch data you have viewed, created, or modified with Okular.
