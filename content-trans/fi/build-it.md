---
menu:
  main:
    name: Koostaminen
    parent: about
    weight: 2
title: Okularin kääntäminen lähdekoodista Linuxissa
---
<span style="background-color:#e8f4fa">Jos etsit esikäännettyjä paketteja, käy [lataussivulla](/download/). Paketoinnin tilan voi tarkistaa [täältä](https://repology.org/project/okular/versions)</span>

Jos Okularin haluaa kääntää, tarvitaan käännösympäristö, jollaisen jakelu yleensä tarjoaa. Haluttaessa kääntää Okularin kehitysversio kannattaa vilkaista KDE:n yhteisöwikistä kohtaa ”Build from source”.

Okularin voi ladata ja kääntää näin:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Jos Okularin asentaa järjestelmän asennuskansiosta poikkeavaan sijaintiin, voi joutua ajamaan ”source build/prefix.sh; okular”, jotta oikea Okular-instanssi ja -kirjastot tulevat valituiksi.

## Valinnaiset paketit

Okularin lisätoiminnallisuutta varten voi asentaa joitakin valinnaisia paketteja. Jotkin on voitu jo paketoida jakeluusi – tai sitten ei. Ongelmia välttääksesi pysyttäydy jakelusi tukemissa paketeissa

* Poppler (PDF-taustaosa): PDF-taustaosan kääntämiseksi tarvitaan [Poppler-kirjasto](http://poppler.freedesktop.org/), jonka vähimmäisversio on 0.24
* Libspectre: Tämän PostScript-taustaosan (PS) kääntämiseen ja käyttämiseen tarvitaan libspectre >= 0.2. Ellei jakelu ole paketoinut sitä tai paketin versio ei ole riittävän tuore, sen voi ladata [täältä](http://libspectre.freedesktop.org/)
* DjVuLibre: DjVu-taustajärjestelmän kääntämiseen vaaditaan DjVuLibre >= 3.5.17. Kuten Libspectren sen saa jakelustasi tai [täältä](http://djvulibre.djvuzone.org/).
* libTIFF: Tätä tarvitaan TIFF/faksi-tukeen. Tällä hetkellä ei vaadita tiettyä versiota, joten mikä hyvänsä jakelun tarjoama tuore kirjaston versio kelvannee. Ongelmatilanteissa älä epäröi ottaa yhteyttä Okularin kehittäjiin.
* libCHM: Tätä vaaditaan CHM-taustajärjestelmän kääntämiseen. Kuten libTIFFillä, sille ei ole vaadittua vähimmäisversiota
* Libepub: ePub-tukea tarvittaessa voi asentaa tämän kirjaston jakelusta tai [SourceForgesta](http://sourceforge.net/projects/ebook-tools).

## Okular-asennuksen poistaminen

Okularin asennuksen voi poistaa ajamalla `make uninstall` `build`-kansiossa. Tämä poistaa sovelluksen järjestelmästä. Tällöin ei poisteta Okularissa katsottuja, luotuja tai muokattuja käyttäjätietoja.
