---
date: 2016-08-18
title: Okular 0.26 julkaistiin
---
Okularin versio 0.26 on julkaistu KDE:n sovellusten 16.08-julkaisuversiossa. Tässä julkaisuversiossa esitellään hyvin pieniä muutoksia; täyden muutoslokin voi nähdä osoitteesta <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Okular 0.26 on suositeltu päivitys kaikille Okularin käyttäjille.
