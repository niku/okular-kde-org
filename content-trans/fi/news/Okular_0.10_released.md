---
date: 2010-02-09
title: Okular 0.10 julkaistiin
---
Okularin versio 0.11 on julkaistu KDE SC 4.4:ssä. Yleisten vakausparannusten lisäksi tässä julkaisussa on uusi tai parannettu tuki sekä tavalliselle että käänteiselle haulle, joka linkittää LaTeX-lähdekoodirivit DVI- ja PDF-tiedostojen vastaaviin kohtiin.
