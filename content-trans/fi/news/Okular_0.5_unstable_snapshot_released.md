---
date: 2006-08-27
title: Okularin epävakaa kehitysversio 0.5 julkaistiin
---
Okularin kehitysryhmä kertoo ylpeänä julkaisseensa Okularista uuden tilannekuvan, joka on tarkoitettu käännettäväksi <a href="http://dot.kde.org/1155935483/">KDE 4 ”Krash” -kehitysversion</a> kanssa. Tilannekuva ei ole vielä täysin käyttökelpoinen, koska hiottavaa ja viimeisteltävää on paljon, mutta kokeile vapaasti ja anna palautetta niin paljon kuin haluat. Tilannekuvapaketin löytää osoitteesta <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2</a>. Tarkista <a href="download.php">lataussivulta</a>, että sinulla on kaikki tarvittavat kirjastot.
