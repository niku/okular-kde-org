---
date: 2008-07-29
title: Okular 0.7 julkaistiin
---
Okular-työryhmä esittelee ylpeänä Okularin uuden version, joka on julkaistu osana <a href="http://www.kde.org/announcements/4.1/">KDE 4.1:tä</a>. Uusia ominaisuuksia ja parannuksia ovat (satunnaisjärjestyksessä):

* Mahdollisuus tallentaa PDF-tiedosto lomakekenttien muutoksineen
* Esitystila: useiden näyttöjen tuki sekä mahdollisuus poistaa siirtymät käytöstä
* Lähennystaso tallennetaan nyt tiedostoittain
* Parannettu puhesynteesituki: voi käyttää koko tiedoston, määräsivun tai tekstivalinnan lukemiseen
* Tekstihaut käänteiseen suuntaan
* Paranneltu lomaketuki
* Alustava (todella perustason ja luultavasti epätäydellinen) tuki PDF-tiedostojen JavaScriptille
* Tiedostoliitemerkintöjen tuki
* Mahdollisuus poistaa sivuilta valkoinen reunus sivuja katseltaessa
* Uusi ePub-tiedostojen taustajärjestelmä
* OpenDocument-tekstitaustajärjestelmä: salattujen tiedostojen tuki sekä luetelma- ja taulukkoparannukset
* XPS-taustajärjestelmä: erilaiset lataus- ja hahmonnusparannukset; on nyt paljon käytettävämpi
* PS-taustajärjestelmä: monet parannukset ennen kaikkea uudemman libspectren ansiosta
