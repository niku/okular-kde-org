---
date: 2017-04-20
title: Okular 1.1 julkaistiin
---
Okularin versio 1.1 on julkaistu KDE:n sovellusten 17.04-julkaisuversiossa. Tässä julkaisuversiossa esitellään mahdollisuus muuttaa merkintöjen kokoa, lomakkeen sisällön automaattisen laskennan tuki, kosketusnäyttöparannuksia ja paljon muuta! Täyden muutoslokin voi nähdä osoitteesta <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.1 on suositeltu päivitys kaikille Okularin käyttäjille.
