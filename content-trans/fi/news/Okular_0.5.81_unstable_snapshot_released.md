---
date: 2006-11-02
title: Okularin epävakaa kehitysversio 0.5.81 julkaistiin
---
Okularin kehitysryhmä kertoo ylpeänä julkaisseensa Okularista uuden tilannekuvan, joka on tarkoitettu käännettäväksi <a href="http://dot.kde.org/1162475911/">toisen KDE 4 -kehitystilannekuvan</a> kanssa. Tilannekuva ei ole vielä täysin käyttökelpoinen, koska hiottavaa ja viimeisteltävää on paljon, mutta kokeile vapaasti ja anna palautetta niin paljon kuin haluat. Tilannekuvapaketin löytää osoitteesta <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Tarkista <a href="download.php">lataussivulta</a>, että sinulla on kaikki tarvittavat kirjastot.
