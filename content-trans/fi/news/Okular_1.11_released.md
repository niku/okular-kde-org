---
date: 2020-08-13
title: Okular 1.11 julkaistiin
---
Okularin versio 1.11 on julkaistu. Tässä julkaisussa esitellään uusi merkintäkäyttöliittymä sekä monia pienempiä yleisiä korjauksia ja piirteitä. Täyden muutoslokin voi nähdä osoitteesta <a href="https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular">https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. Okular 1.11 on suositeltu päivitys kaikille Okularin käyttäjille.
