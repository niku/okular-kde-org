---
date: 2009-04-02
title: ' Okular 0.8.2 julkaistiin'
---
Okular 0.8.2 on julkaistu KDE 4.2-sarjan toisessa korjausjulkaisussa. Tähän julkaisuversioon kuuluu parempi (toivottavasti toimiva) tuki DVI:n sekä pdfsyncin käänteiselle haulle, ja korjauksia sekä pieniä parannuksia esitystilaan. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
