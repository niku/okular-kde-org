---
date: 2017-08-17
title: Okular 1.2 julkaistiin
---
Okularin versio 1.2 on julkaistu KDE:n sovellusten 17.08-julkaisuversiossa. Tässä julkaisuversiossa esitellään pieniä yleisiä virheenkorjauksia ja ominaisuuksia. Täyden muutoslokin voi nähdä osoitteesta <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Okular 1.2 on suositeltu päivitys kaikille Okularin käyttäjille.
