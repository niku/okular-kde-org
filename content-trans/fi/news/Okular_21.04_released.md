---
date: 2021-04-22
title: Okular 21.04 julkaistiin
---
Okularin versio 21.04 on julkaistu. Tässä julkaisussa esitellään PDF-tiedostojen digitaalinen allekirjoitus sekä pienempiä yleisiä virheenkorjauksia ja ominaisuuksia. Täyden muutoslokin voit katsoa osoitteessa <a href='https://kde.org/announcements/releases/21.04.0/#okular'>https://kde.org/announcements/releases/21.04.0/#okular</a>. Okular 21.04 on suositeltu päivitys kaikille Okularin käyttäjille.
