---
date: 2019-12-12
title: Okular 1.9 julkaistiin
---
Okularin versio 1.9 on julkaistu. Tässä julkaisuversiossa esitellään ComicBook-arkistojen cb7-tuki, PDF-tiedostojen JavaScript-tuki sekä useita pienempiä yleisiä virheenkorjauksia ja ominaisuuksia. Täyden muutoslokin voi nähdä osoitteesta <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Okular 1.9 on suositeltu päivitys kaikille Okularin käyttäjille.
