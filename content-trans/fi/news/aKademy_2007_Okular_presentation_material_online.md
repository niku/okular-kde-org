---
date: 2007-07-10
title: aKademy 2007:ssä pidetyn Okularia koskevan esityksen materiaalit verkossa
---
Pino Toscanon Okular-esitelmä <a href="http://akademy2007.kde.org/">aKademy 2007:ssa</a> on nyt verkossa. Saatavilla ovat sekä <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">diat</a> että <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">video</a>.
