---
date: 2012-10-10
title: Okular-foorumi
---
Meillä on nyt KDE:n yhteisöfoorumeissa alifoorumi. Sen löytää osoitteesta <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
