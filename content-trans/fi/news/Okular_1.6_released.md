---
date: 2018-12-13
title: Okular 1.6 julkaistiin
---
Okularin versio 1.6 on julkaistu KDE:n sovellusten 18.12-julkaisuversiossa. Tässä julkaisuversiossa esitellään uusi kirjoituskone-merkintätyökalu sekä monia pieniä yleisiä virheenkorjauksia ja ominaisuuksia. Täyden muutoslokin voi nähdä osoitteesta <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.12.0.php#okular</a>. Okular 1.6 on suositeltu päivitys kaikille Okularin käyttäjille.
