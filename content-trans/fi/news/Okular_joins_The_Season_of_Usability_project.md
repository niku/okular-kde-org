---
date: 2007-01-31
title: Okular liittyy The Season of Usability -projektiin
---
Okularin kehitysryhmä kertoo ylpeänä, että Okular on valittu osallistumaan <a href="http://www.openusability.org/season/0607/">Season of Usability</a>  -hankkeeseen, jota vetävät <a href="http://www.openusability.org">OpenUsabilityn</a> käytettävyysasiantuntijat. Haluamme toivottaa Sharad Baliyanin tervetulleeksi kehitysryhmäämme ja kiittää Florian Graessleä ja Pino Toscanoa heidän jatkuvasta työstään Okularin parantamiseksi.
