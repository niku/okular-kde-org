---
date: 2007-04-04
title: Okular kdegraphicsissa
---
Okularin kehitysryhmä kertoo ylpeänä, että Okular on nyt osa kdegraphics-moduulia. Okularin seuraava versio toimitetaan KDE 4.0:n mukana.
