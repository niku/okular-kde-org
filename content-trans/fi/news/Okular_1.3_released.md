---
date: 2017-12-14
title: Okular 1.3 julkaistiin
---
Okularin versio 1.3 on julkaistu KDE:n sovellusten 17.12-julkaisuversiossa. Tässä julkaisuversiossa esitellään muutoksia siihen, miten merkinnät tallennetaan ja miten lomakkeet toimivat, osittaishahmonnustuki hitaasti hahmonnettaville tiedostoille, vuorovaikutteiset tekstilinkit tekstivalintatilassa, Tiedosto-valikon uusi Jaa-kohta, Markdown-tuki sekä korjauksia HiDPI-tuen ongelmiin. Täyden muutoslokin voi nähdä osoitteesta <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0.php#okular</a>. Okular 1.3 on suositeltu päivitys kaikille Okularin käyttäjille.
