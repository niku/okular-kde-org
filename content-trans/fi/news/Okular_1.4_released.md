---
date: 2018-04-19
title: Okular 1.4 julkaistiin
---
Okularin versio 1.4 on julkaistu KDE:n sovellusten 18.04-julkaisuversiossa. Tässä julkaisuversiossa esitellään PDF-tiedostojen parannettu JavaScript-tuki sekä PDF-hahmonnuksen perumisen tuki, mikä tarkoittaa, että kun monimutkaista PDF-tiedostoa yrittää lähentää sen vielä hahmontuessa, hahmonnus peruuntuu heti eikä vasta valmistuttuaan. Täyden muutoslokin voi nähdä osoitteesta <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0.php#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0.php#okular</a>. Okular 1.4 on suositeltu päivitys kaikille Okularin käyttäjille.
