---
date: 2020-04-23
title: Okular 1.10 julkaistiin
---
Okularin versio 1.10 on julkaistu. Tässä julkaisussa esitellään kineettinen vieritys, välilehtihallinnan ja mobiilikäyttöliittymän parannuksia sekä monia pienempiä yleisiä virheenkorjauksia ja ominaisuuksia. Täyden muutoslokin voi nähdä osoitteesta <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases?version=20.04.0#okular</a>. Okular 1.10 on suositeltu päivitys kaikille Okularin käyttäjille.
