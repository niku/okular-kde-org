---
date: 2015-12-16
title: Okular 0.24 julkaistiin
---
Okularin versio 0.24 on julkaistu KDE:n sovellusten 15.12-julkaisuversiossa. Tämä julkaisuversio esittelee pieniä virhekorjauksia ja ominaisuuksia. Täyden muutoslokin voit katsoa osoitteessa <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 on suositeltu päivitys kaikille Okularin käyttäjille.
