---
date: 2020-12-10
title: Okular 20.12 julkaistiin
---
Okularin versio 20.12 on julkaistu. Tässä julkaisuversiossa esitellään pieniä yleisiä virhekorjauksia ja ominaisuuksia. Täyden muutoslokin voi nähdä osoitteesta <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0.php#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0.php#okular</a>. Okular 20.12 on suositeltu päivitys kaikille Okularin käyttäjille.
