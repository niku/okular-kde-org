---
date: 2018-08-16
title: Okular 1.5 julkaistiin
---
Okularin versio 1.5 on julkaistu KDE:n sovellusten 18.08-julkaisuversiossa. Tässä julkaisuversiossa esitellään lomakeparannuksia monien pienempien yleisten virheenkorjausten ja ominaisuuksien ohessa. Täyden muutoslokin voi nähdä osoitteesta <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0.php#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0.php#okular</a>. Okular 1.5 on suositeltu päivitys kaikille Okularin käyttäjille.
