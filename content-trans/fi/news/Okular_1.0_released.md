---
date: 2016-12-15
title: Okular 1.0 julkaistiin
---
Okularin versio 1.0 on julkaistu KDE:n sovellusten 16.12-julkaisuversiossa. Tämä julkaisu perustuu KDE Frameworks 5:een. Täyden muutoslokin voi nähdä osoitteesta <a href='https://www.kde.org/announcements/fulllog_applications-16.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-16.12.0.php#okular</a>. Okular 1.0 on suositeltu päivitys kaikille Okularin käyttäjille.
