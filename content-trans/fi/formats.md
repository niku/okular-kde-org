---
intro: Okular supports a wide variety of document formats and use cases.
layout: formats
menu:
  main:
    name: Tiedostomuoto
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Tiedostomuotojen käsittelyn tila
---
