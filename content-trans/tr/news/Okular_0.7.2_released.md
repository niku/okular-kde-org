---
date: 2008-10-03
title: Okular 0.7.2 yayımlandı
---
KDE 4.1 serisinin ikinci bakım sürümü Okular 0.7.2'yi içeriyor. Bu sürüm TIFF ve Comicbook arka uçlarında bazı küçük düzeltmeleri ve "Genişliğe Sığdır" seçeneğinin öntanımlı yakınlaştırma seviyesi olması değişimini içeriyor. Düzeltilen tüm durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a> adresinde okuyabilirsiniz.
