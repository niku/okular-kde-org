---
date: 2009-06-03
title: Okular 0.8.4 yayımlandı
---
KDE 4.2 serisinin dördüncü bakım sürümü Okular 0.8.4'ü içeriyor. Bu sürüm OpenDocument metin belgelerinde bazı düzeltmeler, birkaç çökme düzeltmesi ve arayüzde bazı küçük hata düzeltmeleri içeriyor. Düzeltilen tüm durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a> adresinde okuyabilirsiniz.
