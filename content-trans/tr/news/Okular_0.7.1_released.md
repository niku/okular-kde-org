---
date: 2008-09-03
title: Okular 0.7.1 yayımlandı
---
KDE 4.1 serisinin ilk bakım sürümü Okular 0.7.1'i içeriyor. Bu sürüm bazı çökme düzeltmelerinin yanı sıra bazı küçük düzeltmeleri içeriyor. Düzeltilen tüm durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a> adresinde okuyabilirsiniz.
