---
date: 2013-08-16
title: Okular 0.17 yayımlandı
---
Okular'ın 0.17 sürümü KDE uygulamalarının 4.11 sürümüyle birlikte yayımlandı. Bu sürüm formlar ve yorumlar için geri alma/yineleme özelliği ve yapılandırılabilir inceleme araçları gibi yeni özellikler içeriyor. Okular 0.17, Okular kullanan herkes için önerilen bir güncellemedir.
