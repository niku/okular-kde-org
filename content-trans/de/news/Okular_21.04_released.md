---
date: 2021-04-22
title: Okular 21.04 veröffentlicht
---
Die Version 21.04 von Okular wurde veröffentlicht. Diese Veröffentlichung enthält digitale Signaturen für PDF-Dateien sowie Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://kde.org/announcements/changelog-releases.php?version=21.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=21.04.0#okular</a>. Okular 21.04 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
