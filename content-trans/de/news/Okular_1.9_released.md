---
date: 2019-12-12
title: Okular 1.9 veröffentlicht
---
Die Version 1.9 von Okular wurde veröffentlicht. Diese Veröffentlichung enthält cb7-Unterstützung für ComicBook-Archive,Verbesserungen der JavaScript-Unterstützung für PDF-Dateien sowie Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=19.12.0#okular</a>. Okular 1.9 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
