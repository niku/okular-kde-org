---
date: 2021-08-12
title: Okular 21.08 veröffentlicht
---
Die Version 21.08 von Okular wurde veröffentlicht. Diese Veröffentlichung enthält kleinere Fehlerkorrekturen und allgemeine Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https://kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. Okular 21.08 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
