---
date: 2008-07-29
title: Okular 0.7 veröffentlicht
---
Das Okular-Team freut sich, eine neue Version von Okular ankündigen zu können, die als Teil von <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a> veröffentlicht wurde. In dieser Version finden Sie die Folgenden neuen Funktionen und Verbesserungen

* PDF-Dokumente können mit Eingaben in den Formularfelder gespeichert werden.
* Präsentationsmodus: Unterstützung für mehrere Bildschirme, Übergänge können abgeschaltet werden
* Die zuletzt verwendete Vergößerungsstufe der Ansicht wird für jedes Dokument gespeichert
* Verbesserungen bei der Sprachausgabe, das ganze Dokument, eine einzelne Seite oder eine Textauswahl kann vorgelesen werden
* Rückwärtssuche nach Text
* Verbesserte Unterstützung für Formulare
* Vorläufige und unvollständige Unterstützung von JavaScript in PDF-Dokumenten
* Unterstützung für Anmerkungen in Dateianhängen
* Unbedruckte Ränder können bei der Ansicht der Seiten entfernt werden
* Neues Anzeigemodul für EPub-Dokumente
* OpenDocument-Anzeigemodul: Unterstützung für verschlüsselte Dokumente und Verbesserungen für Listen und Tabellen.
* XPS-Anzeigemodul: Verschiedene Verbesserungen für da Laden und Rendern
* PS-Anzeigemodul: Verschiedene Verbesserungen, hauptsächlich wegen einer neueren Bibliothek libspectre
