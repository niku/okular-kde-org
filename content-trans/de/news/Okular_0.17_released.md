---
date: 2013-08-16
title: Okular 0.17 veröffentlicht
---
Die Version 0.17 von Okular wurde zusammen mit den KDE-Programmen 4.11 veröffentlicht. Diese Veröffentlichung enthält neue Funktionen wie Unterstützung von Rückgängig/Wiederherstellen für Formulare und Anmerkungen und einstellbare Werkzeuge für Rezensionen. Okular 0.17 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
