---
intro: Okular ist als kompiliertes Paket für viele Plattformen verfügbar. Den Status
  der Pakete für Ihre Linux-Distribution finden Sie auf der rechten Seite. Informationen
  zu anderen Betriebssystemen finden Sie weiter unten.
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular ist bereits für die meisten Linux-Distributionen verfügbar. Sie können
    es auf der [Seite der KDE-Anwendungen](https://apps.kde.org/okular) installieren.
- image: /images/flatpak.png
  image_alt: Flatpak-Logo
  name: Flatpak
  text: Sie können auch die neueste Version als [Okular-Flatpak](https://flathub.org/apps/details/org.kde.okular)
    von Flathub installieren. Experimentell Flatpak-Pakete mit täglich neuen Versionen
    von Okular können Sie aus dem [KDE-Flatpack-Archiv](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications)
    installieren.
- image: /images/ark.svg
  image_alt: Ark-Logo
  name: Veröffentlichung der Quelltexte
  text: Okular wird regelmäßig als Teil von KDE Gear veröffentlicht. Wenn Sie Okular
    aus den Quelltexten erstellen möchten, gehen Sie zu dieser [Seite](/build-it).
- image: /images/windows.svg
  image_alt: Windows-Logo
  name: Windows
  text: Auf der Webseite der [„KDE on Windows“-Initiative](https://community.kde.org/Windows)
    finden Sie Informationen über die Installation von KDE-Software auf Windows. Die
    stabile Version ist im [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8)
    verfügbar. Dort finden Sie [experimentelle Installationsdateien](https://binary-factory.kde.org/job/Okular_Nightly_win64/).
    Testen Sie Okular und senden Sie Fehlerberichte.
sassFiles:
- /sass/download.scss
title: Herunterladen
---
## Installation

To install Okular follow the instructions of the platform you are using for how to install software. Most platforms have a graphical installer which can be used to install software packages. In many cases it will automatically be opened when clicking the download or install link for your platform.

If you are using Linux, Okular might already be pre-installed on your system as part of a default selection. If not, you can install it with the package management tool of your Linux distribution. See its documentation for details.

In many cases you will have the opportunity to install Okular in a modular way, so you can decide to install support for specific formats separately or install translations for the user interface. Choose the modules according to your needs and preferences.

## Uninstallation

To uninstall Okular follow the instructions of the package management tool you have used to install Okular. This will remove the Okular application. It will not touch data you have viewed, created, or modified with Okular.
