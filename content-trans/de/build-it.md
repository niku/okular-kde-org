---
menu:
  main:
    name: Erstellen
    parent: about
    weight: 2
title: Okular aus dem Quelltext unter Linux kompilieren
---
<span style="background-color:#e8f4fa">Suchen Sie nach kompilierten Paketen, dann gehen Sie zur [Download-Seite](/download/). Den Status der Pakete finden Sie  [hier](https://repology.org/project/okular/versions)</span>

Wenn Sie Okular selbst kompilieren möchten, brauchen Sie Entwicklungsumgebung, die Sie von Ihrer Distribution erhalten. Falls Sie die Entwicklungsversionen von Okular kompilieren möchten, halten Sie sich an die Anweisungen im KDE-Community-Wiki.

Sie können Okular so herunterladen und kompilieren:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Falls Sie Okular in einem anderen Pfad als Ihre systemweite Installation installieren, ist es möglicherweise nötig, „source build/prefix.sh; okular“ auszuführen, damit die richtigen Instanzen von Okular und den Bibliotheken verwendet werden.

## Optionale Pakete

Es gibt einige optionale Pakete, die Sie für weitere Funktionen in Okular installieren können. Möglicherweise sind einige Pakete bereits in Distribution enthalten. Wenn Sie Probleme vermeiden wollen, benutzen Sie die Pakete Ihrer Distribution.

* Poppler (PDF -Anzeigemodul): Um das PDF-Anzeigemodul zu kompilieren, wird die [Poppler-Bibliothek](http://poppler.freedesktop.org) benötigt, mindestens die Version 0.24.
* Libspectre: Um dieses PostScipt-(PS)-Anzeigemodul zu kompilieren und zu benutzen, brauchen Sie libspectre >= 0.2. Falls Ihre Distribution keine Pakete von libspectre anbietet oder die angebotene Version nicht aktuell genug ist, können Sie libspectre [hier](http://libspectre.freedesktop.org) herunterladen.
* DjVuLibre: Um dieses DjVu-Anzeigemodul zu kompilieren, brauchen Sie DjVuLibre >= 3.5.17.Wie bei Libspectre erhalten Sie es über Ihre Distribution oder können es [hier](http:/djvulibre.djvuzone.org) herunterladen.
* libTIFF: Wird für das TIFF/Fax-Anzeigemodul benötigt. Derzeit ist keine Mindestversion dafür vorgeschrieben, sodass jede einigermaßen aktuelle Version der von Ihrer Distribution bereitgestellten Bibliothek funktionieren sollte. Falls es damit Probleme gibt, zögern Sie nicht, die Okular-Entwickler zu kontaktieren.
* libCHM: Wird für das CHM-Anzeigemodul benötigt. Wie bei libTIFF ist keine Mindestversion dafür vorgeschrieben
* Libepub: Brauchen Sie Unterstützung für EPub, können Sie diese Bibliothek über Ihre Distribution installieren oder von [Sourceforge](http://sourceforge.net/projects/ebook-tools) herunterladen.

## Okular deinstallieren

Sie können Okular deinstallieren, indem Sie „make uninstall“ in Ihrem „build“-Ordner ausführen. Dadurch wird die Anwendung von Ihrem System entfernt. Es werden keine Benutzerdaten entfernt, die Sie mit Okular angesehen, erstellt oder geändert haben.
