---
intro: Okular stöder ett stort antal dokumentformat och användarfall.
layout: formats
menu:
  main:
    name: Dokumentformat
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Status för hantering av dokumentformat
---
