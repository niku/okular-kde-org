---
date: 2021-08-12
title: Okular 21.08 utgiven
---
Version 21.08 av Okular har givits ut. Utgåvan innehåller diverse mindre felrättningar och funktionsförbättringar överallt. Du kan titta på den fullständiga ändringsloggen på <a href='https://kde.org/announcements/changelogs/gear/21.08.0/#okular'>https://kde.org/announcements/changelogs/gear/21.08.0/#okular</a>. Okular 21.08 är en rekommenderad uppdatering för alla som använder Okular.
