---
date: 2008-07-29
title: Okular 0.7 utgiven
---
Okular-gruppen är stolt över att tillkännage en ny version av Okular, utgiven som en del av <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Vissa av de nya funktionerna och förbättringarna omfattar (i slumpmässig ordning):

* Möjlighet att spara ett PDF-dokument med ändringarna i formulärfält
* Presentationsläge: stöd för mer än en skärm, och möjlighet att inaktivera övergångar
* Zoomnivån lagras nu per dokument
* Förbättrat stöd för text-till-tal, som kan användas för att läsa upp hela dokumentet, en specifik sida eller den markerade texten
* Bakåtriktning för textsökning
* Förbättrat stöd för formulär
* Preliminärt (mycket grundläggande och troligtvis ofullständigt) stöd för Javascript i PDF-dokument
* Stöd för kommentarer i filbilaga
* Möjlighet att ta bort den vita kanten för sidor vid visning av sidor
* Nytt gränssnitt för EPub-dokument
* OpenDocument-textgränssnitt: stöd för krypterade dokument, förbättringar av listor och tabeller
* XPS-gränssnitt: diverse förbättringar för inläsning och återgivning, bör vara mycket användbarare nu
* PS-gränssnitt: diverse förbättringar, i huvudsak på grund av det nyare libspectre som krävs
