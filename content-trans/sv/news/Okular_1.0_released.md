---
date: 2016-12-15
title: Okular 1.0 utgiven
---
Version 1.0 av Okular har givits ut tillsammans med utgåva 16.12 av KDE:s program. Utgåvan är nu baserad på KDE Ramverk 5. Den fullständiga ändringsloggen finns på <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Okular 1.0 är en rekommenderad uppdatering för alla som använder Okular.
