---
date: 2008-04-02
title: Okular 0.6.3 utgiven
---
Den tredje underhållsutgåvan i KDE 4.0-serien inkluderar Okular 0.6.3. Den innehåller några felrättningar, t.ex. ett bättre sätt att hämta textpositionen i ett PDF-dokument, och några rättningar av kommentarsystemet och innehållsförteckningen. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
