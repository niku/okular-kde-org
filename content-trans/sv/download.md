---
intro: Okular är tillgängligt som ett förkompilerat paket på ett stort antal plattformar.
  Du kan kontrollera paketstatus för din Linux-distribution till höger eller fortsätta
  läsa för information om andra operativsystem.
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular är redan tillgängligt på de flesta Linux-distributioner. Du kan installera
    det från [KDE:s programvarucentral](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Flatpaks logotyp
  name: Flatpak
  text: Du kan installera senaste [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)
    från Flathub. Experimentella Flatpak-paket med nattliga byggen av Okular kan [installeras
    från KDE:s Flatpak-arkiv](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Arks logotyp
  name: Källor för utgåvor
  text: Okular ges ut regelbundet som en del av KDE Gear. Om du vill bygga från källkod,
    kan du titta i [avsnittet Bygga den](/build-it).
- image: /images/windows.svg
  image_alt: Windows logotyp
  name: Windows
  text: Ta en titt på initiativet [KDE på Windows](https://community.kde.org/Windows)
    webbsida för information om hur man installerar KDE-programvara på Windows. Den
    stabila utgåvan är till gänglig på [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    Det finns också [experimentella nattliga byggen](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    där tester och felrapporter välkomnas.
sassFiles:
- /sass/download.scss
title: Nerladdning
---
## Installation

För att installera Okular följ instruktionerna i pakethanteringsverktyget du använder för att installera programvara. De flesta plattformar har ett grafiskt installationsverktyg som kan användas för att installera programvarupaket. I många fall öppnas det automatiskt när nerladdnings. eller installationslänken för plattformen klickas.

Om du använder Linux, kanske Okular redan är förinstallerat på systemet som en del av ett förvalt urval. Om inte, kan du installera den med Linux-distributionens pakethanteringsverktyg. Se dess dokumentation för detaljerad information.

I många fall har du möjlighet att installera Okular på ett modulärt sätt, så att du kan välja att installera stöd för specifika format separat, eller installera översättningar av användargränssnittet. Välj modulerna enligt dina behov och önskemål.

## Avinstallation

För att avinstallera Okular följ instruktionerna i pakethanteringsverktyget som du har använt för att installera Okular. Det tar bort programmet Okular. Det tar inte bort någon användardata som du har tittat på, skapat, eller ändrat med Okular.
