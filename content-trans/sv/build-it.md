---
menu:
  main:
    name: Bygga den
    parent: about
    weight: 2
title: Kompilera Okular från källkod på Linux
---
<span style="background-color:#e8f4fa">Om du letar efter förkompilerade paket, besök [nerladdningssidan](/download/). Du kan kontrollera paketstatus [här](https://repology.org/project/okular/versions)</span>

Om du vill kompilera Okular, måste du ha en kompileringsmiljö inställd , vilket i allmänhet bör tillhandahållas av din distribution. Om du vill kompilera utvecklingsversionen av Okular, se "Bygg från källkod" på KDE-gemenskapens Wiki.

Du kan ladda ner och kompilera Okular på följande sätt:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Om du installerar Okular med en annan sökväg än systemets installationskatalog är det möjligt att du behöver köra `source build/prefix.sh; okular` så att rätt instans av Okular och bibliotek används.

## Valfria paket

Det finns några valfria paket som du kan installera för att få mer funktioner i Okular. Vissa kanske redan är paketerade i din distribution, men andra kanske inte är det. Om du vill undvika eventuella problem, håll dig till paket som stöds av distributionen.

* Poppler (PDF-gränssnitt): För att kompilera PDF-gränssnittet, behövs [biblioteket Poppler](http://poppler.freedesktop.org), där den tidigaste versionen som krävs är 0.24.
* Libspectre: För att kompilera och använda detta Postscript (PS)-gränssnitt behövs libspectre ≥ 0.2. Om din distribution inte paketerar det, eller den paketerade versionen inte är tillräcklig, kan det laddas ner [här](http://libspectre.freedesktop.org).
* DjVuLibre: För att kompilera DjVu-gränssnittet, behöver du DjVuLibre ≥ 3.5.17. Liksom med Libspectre, kan du hämta det från din distribution eller [här](http://djvulibre.djvuzone.org).
* libTIFF: Det behövs för att kompilera stöd för TIFF/telefax. För närvarande finns det ingen tidigaste version som krävs, så vilken som helst ganska ny version av biblioteket från distributionen bör fungera. Vid problem, tveka inte att kontakta Okulars utvecklare.
* libCHM: Det behövs för att kompilera CHM-gränssnittet. Liksom med libTIFF, finns det inga krav på tidigaste version.
* Libepub: Om du behöver stöd för EPub, kan du installera biblioteket från din distribution eller från [sourceforge](http://sourceforge.net/projects/ebook-tools).

## Avinstallera Okular

Du kan avinstallera Okular genom att köra `make uninstall` i katalogen `build`. Det tar bort programmet från systemet. Det tar inte bort någon användardata som du har tittat på, skapat, eller ändrat med Okular.
